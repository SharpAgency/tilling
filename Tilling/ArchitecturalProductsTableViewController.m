//
//  ArchitecturalProductsTableViewController.m
//  Tilling
//
//  Created by Sharp Agency on 28/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ArchitecturalProductsTableViewController.h"
#import "ArchitecturalTableViewCell.h"

@implementation ArchitecturalProductsTableViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellBackgroundColorArray;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.898 green:0.478 blue:0.000 alpha:1.000];

    cellTitles = @[@"Lining & Cladding", @"Pre-Finished", @"SmartPrime", @"Block Wall - SilhouetteTM", @"Stepped Profile – ContourTM", @"Stepped Profile – HorizonTM"];
    
    cellBackgroundColorArray = @[[UIColor colorWithRed:203/255.0 green:82/255.0 blue:0 alpha:0.600],
                                 [UIColor colorWithRed:213/255.0 green:92/255.0 blue:0 alpha:0.600],
                                 [UIColor colorWithRed:223/255.0 green:102/255.0 blue:0 alpha:0.600],
                                 [UIColor colorWithRed:233/255.0 green:112/255.0 blue:0 alpha:0.600],
                                 [UIColor colorWithRed:243/255.0 green:122/255.0 blue:0 alpha:0.600],
                                 [UIColor colorWithRed:253/255.0 green:132/255.0 blue:0 alpha:0.600]];
    
    cellDetailText = @[@"Tilling are proud to announce the new, comprehensive, all-in-one Architectural Products portfolio. Tilling have combined the internal, external and pre-finished product ranges into one major category. The new \"Architectural Products\" brochure provides time saving by housing all products in the one document. No longer do you have to wade through 3 different publications to find what you want. Now, just use the easy index in the front and turn to the required page.\n\nWestern Red Cedar has one of the longest life spans of any North American Softwood. It produces long lengths of timber with true, straight grain. Light weight, easy to work with, easy to finish, possessing outstanding dimensional stability, cedar is a preferred wood for nearly all purposes where attractive appearance or resistance to weather is important.\n\nTilling produce an extensive range of Raw and Pre-finished architecturally designed interior lining boards. Tilling Pre-finished Lining Boards create an instant impression on interior walls and ceilings. They’re perfectly finished with 4 coats of satin ‘Ultra Violet ‘cured polyurethane ready to install. The Pre-finished range now comes in three timber species giving you a choice in both colour and texture to complement your interior space. And, at Tilling, we source timber from sustainably managed and harvested forests in Australia and across the globe.\n\nAll kiln-seasoned premium grade/tight knot cedar claddings are available in lengths from 0.6 m to 4.8 m (longer lengths may be available). All cladding is protective packaged in clear poly shrink film, with each bundle carrying four or six same length boards. “CP” series cladding are an economic option to traditional 18 mm profiles packaged six board per bundle.\n\nPremium grade quality permits inclusion of natural wood characteristics such as small, sound pin knots or burls in occasional boards. Channel claddings are also available with optional fine sawn textured face for a small surcharge.",
                       
                       @"Tilling are proud to announce the new, comprehensive, all-in-one Architectural Products portfolio. Tilling have combined the internal, external and pre-finished product ranges into one major category. The new \"Architectural Products\" brochure provides time saving by housing all products in the one document. No longer do you have to wade through 3 different publications to find what you want. Now, just use the easy index in the front and turn to the required page.\n\nWestern Red Cedar has one of the longest life spans of any North American Softwood. It produces long lengths of timber with true, straight grain. Light weight, easy to work with, easy to finish, possessing outstanding dimensional stability, cedar is a preferred wood for nearly all purposes where attractive appearance or resistance to weather is important.\n\nTilling produce an extensive range of Raw and Pre-finished architecturally designed interior lining boards. Tilling Pre-finished Lining Boards create an instant impression on interior walls and ceilings. They’re perfectly finished with 4 coats of satin ‘Ultra Violet ‘cured polyurethane ready to install. The Pre-finished range now comes in three timber species giving you a choice in both colour and texture to complement your interior space. And, at Tilling, we source timber from sustainably managed and harvested forests in Australia and across the globe.\n\nAll kiln-seasoned premium grade/tight knot cedar claddings are available in lengths from 0.6 m to 4.8 m (longer lengths may be available). All cladding is protective packaged in clear poly shrink film, with each bundle carrying four or six same length boards. “CP” series cladding are an economic option to traditional 18 mm profiles packaged six board per bundle.\n\nPremium grade quality permits inclusion of natural wood characteristics such as small, sound pin knots or burls in occasional boards. Channel claddings are also available with optional fine sawn textured face for a small surcharge.",
                       
                       @"New from Tilling Timber is the comprehensive range of SmartPrime™ Architectural Profiles.\n\nSmartPrime™ offers a comprehensive range of architectural designer pre-primed profiles for external use. The SmartPrime™ range of products are manufactured in accordance with all current Australian standards, including the finger jointed and lamination processes.\n\nSmartPrime™ architectural profiles have been treated with H3 ‘Light Organic Solvent Preservatives’ (LOSP) to offer maximum protection against the elements and termites.\n\nFixing into SmartPrime™ profiles using hot-dipped galvanised, high tensile aluminium or stainless steel fasteners is advisable. All laminated SmartPrime™ products are manufactured using nominated adhesives as stipulated within the current, applicable, Australian Standards.\n\nSmartPrime™ architectural profiles are produced from Kiln Dried (KD) material and Finger Jointed with a ‘Micro-Joint’ making it extremely durable.\n\nSmartPrime™ has been prepared and primed to combat the harsh conditions of our Australian climate. Following the instructions provided will ensure the best possible protection against continuous external exposure.\n\nPlease consult the SmartPrime™ brochure to seek the optimal advice on painting the profiles, as incorrect application of paint may result in voiding the products warranty.\n\nTilling developed SmartPrime™ because they understand that the cost of timber is not the only consideration when it comes to building a project. The benefits of using SmartPrime™ are easy to see. SmartPrime™ provides a high quality, paint-ready, consistent and reliable range of profiles, sold in lengths that make for convenient installation on site.",
                       
                       @"Block Wall - SilhouetteTM has been created to provide extra depth for the most dramatic shadow line effect possible. Available in Western Red Cedar and Tasmanian Blackwood (run to order only). The profiles integrate with many other Tilling profiles to provide a truly unique customised design opportunity. Individual boards can be installed ‘end on end’ to create a continuous but dramatic undulating effect.\n\nAlternatively boards can be docked to shorter lengths and mixed and matched for a totally random look. All profiles are secret nailed for that perfect finish. This random look will also provide excellent acoustic performance so it is ideal for music rooms or in any situation where noise is an issue. Suitable for both internal and external installation. Block Wall - SilhouetteTM is also available in ready to install 1200x300mm panels in Western Red Cedar.\n\nThese panels have been manufactured to create the random look but in convenient modules to save on site labour you can achieve that handcrafted end result in a fraction of the time that it would normally take. Each panel is tongued and grooved to provide continuity of finish and surety of installation. Panels are secret nailed for that perfect finish and are suitable only for internal installation. Refer to Tilling website for fixing instructions and a step by step video for installation of the Block Wall - SilhouetteTM panels.\n\nBlock Wall - SilhouetteTM is the ideal feature wall and ceiling product for use in a multitude of domestic, civic and commercial applications.\n- Feature walls\n- Alfresco ceilings\n- Music Rooms\n- Home Theatres\n- Retail entrance and display walls",
                       
                       @"A unique male to male and female to female connection system, allowing the profiled boards to be rotated 180 degrees, and providing another dimension to the patterns you can create.\n\nThis profile integrates with many other profiles to provide a truly unique customised design opportunity.\n\nStepped Profile – ContourTM is the ideal feature wall and ceiling product for use in a multitude of domestic and commercial applications.\n\n- Lounge room feature walls\n- Alfresco ceilings\n- Retail entrance and feature walls\n- Library and other quiet environments\n- Office and hotel reception areas\n- Architectural feature surrounds (plaster borders)\n- Feature walls in a boardroom\n- Bar and restaurant feature walls ",
                       
                       @"Stepped Profile – HorizonTM profiles provide a bold, continuous & linear appearance with strong shadow lines. With a choice of four castellation profiles in varying widths & depths. You can create a multitude of different looks through a mixing and matching process.\n\nThese profiles can be intermixed with profiles of the same thickness providing an architecturally unique statement to your feature wall or ceiling. All proles can be secret nailed and are suitable for both internal & external installation. Stepped Profile – HorizonTM is the ideal feature wall and ceiling product for use in a multitude of domestic, civic and commercial applications.\n\n- Feature walls\n- Alfresco ceilings\n- Music Rooms\n- Home Theatres\n- Retail entrance and display walls"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [cellTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.row];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [cellBackgroundColorArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            [self performSegueWithIdentifier:@"productProfileSegue" sender:nil];
        }
    }
    if (indexPath.section == 1) {
        [self performSegueWithIdentifier:@"productsSegue" sender:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 44 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.row == 0) return 950; // lining
        if (indexPath.row == 1) return 920; // lustre
        if (indexPath.row == 2) return 900; // smartprime
        if (indexPath.row == 3) return 800; // silhouette
        if (indexPath.row == 4) return 480; // contour
        if (indexPath.row == 5) return 480; // horizon
    }
    
    return 56;
}



#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
