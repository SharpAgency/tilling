//
//  ArchitecturalTableViewCell.h
//  Tilling
//
//  Created by Sharp Agency on 28/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchitecturalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property BOOL noShadow;


@end
