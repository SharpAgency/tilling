//
//  BearersCalcViewController.m
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "BearersCalcViewController.h"
#import "CustomCalculatorCell.h"
#import "BearersDetailsViewController.h"

@interface BearersCalcViewController ()

@end

@implementation BearersCalcViewController {
    NSDictionary *detailsContent;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if ([_content count]) return [_content count];
    else return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set Cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // if content exists, display it................
    if ([_content count]) {
        CustomCalculatorCell *cell = (CustomCalculatorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        // If no cell, create one with custom nib
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomCalculatorCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        cell.sizeLabel.text = [NSString stringWithFormat:@"%@", [[_content objectAtIndex:indexPath.row] valueForKey:@"DxB"]];
        cell.spanLabel.text = [[_content objectAtIndex:indexPath.row] valueForKey:@"span"];
        cell.ebLabel.text = [[_content objectAtIndex:indexPath.row] valueForKey:@"EB"];
        
        // check if row is odd or even and set color accordingly
        if (indexPath.row % 2) {
            cell.backgroundColor = [UIColor colorWithRed:0.408 green:0.840 blue:0.938 alpha:0.600];
        }else {
            cell.backgroundColor = [UIColor colorWithRed:0.086 green:0.720 blue:0.898 alpha:0.600];
        }
        
        return cell;
    }
    // if content doesnt exist, inform the user.................
    else {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.text = @"No results within bounds of calculation";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
        
        tableView.separatorColor = [UIColor clearColor];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    detailsContent = [_content objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"detailsSegue" sender:nil];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([_content count]) {
        UIView *colorBack = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        colorBack.backgroundColor = [UIColor colorWithRed:0.086 green:0.685 blue:0.848 alpha:1.000];
        UILabel *label = [[UILabel alloc] initWithFrame:colorBack.frame];
        label.text = @"    Size                  Span                     EB";
        label.textColor = [UIColor whiteColor];
        [colorBack addSubview:label];
        
        return colorBack;
    }
    return nil;
}


#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"detailsSegue"]) {
        BearersDetailsViewController *detailsView = [segue destinationViewController];
        [detailsView setContent:detailsContent];
    }
}


@end
