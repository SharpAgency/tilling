//
//  BearersDetailsViewController.m
//  Tilling
//
//  Created by Beau Young on 24/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "BearersDetailsViewController.h"
#import <MessageUI/MessageUI.h>

@interface BearersDetailsViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation BearersDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    if ([[_content objectForKey:@"productID"] isEqualToString:@"1"]) self.productLabel.text = @"SmartLvl15";
    else if ([[_content objectForKey:@"productID"] isEqualToString:@"3"]) self.productLabel.text = @"SmartLam GL13c";
    else if ([[_content objectForKey:@"productID"] isEqualToString:@"4"]) self.productLabel.text = @"SmartLam GL17c";
    else if ([[_content objectForKey:@"productID"] isEqualToString:@"5"]) self.productLabel.text = @"SmartLam GL18c";

    self.sizeLabel.text = [_content objectForKey:@"DxB"];
    
    if ([[_content objectForKey:@"spanConditionID"] isEqualToString:@"1"]) self.spanConditionLabel.text = @"Single Span";
    else if ([[_content objectForKey:@"spanConditionID"] isEqualToString:@"2"]) self.spanConditionLabel.text = @"Continuous Span";
    
    self.spanLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"span"]];
    
    self.deadloadLabel.text = [NSString stringWithFormat:@"%@kg/m2", [_content objectForKey:@"FloorDLMass"]];
    
    if ([[_content objectForKey:@"FllFplID"] isEqualToString:@"1"]) self.liveLoadLabel.text = @"1.5kPa/1.8kN";
    else if ([[_content objectForKey:@"FllFplID"] isEqualToString:@"2"]) self.liveLoadLabel.text = @"2kPa/1.8kN";
    else if ([[_content objectForKey:@"FllFplID"] isEqualToString:@"3"]) self.liveLoadLabel.text = @"3kPa/1.7kN";
    
    self.loadWidthLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"floor_load_width"]];
    
    self.endBearingLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"EB"]];
    if (![[_content objectForKey:@"IB"] isEqualToString:@""]) {
        self.internalBearingLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"IB"]];
    }
    else self.internalBearingLabel.text = @"0mm";
    
}
- (IBAction)emailBtn:(UIBarButtonItem *)sender {
    // email subject
    NSString *emailSubject = @"Tilling Building Calculator - Your Results";
    NSString *messageBody = [NSString stringWithFormat:@"Product: %@\n Size DxB (mm): %@\n Span Condition: %@\n Span: %@\n Floor Dead Load: %@\n Floor Live Load/Floor Point Load: %@\n Floor Load Width: %@\n End Bearing Length Required: %@\n Internal Bearing Length Required: %@\n",
                             _productLabel.text,
                             _sizeLabel.text,
                             _spanConditionLabel.text,
                             _spanLabel.text,
                             _deadloadLabel.text,
                             _liveLoadLabel.text,
                             _loadWidthLabel.text,
                             _endBearingLabel.text,
                             _internalBearingLabel.text];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailSubject];
    [mc setMessageBody:messageBody isHTML:NO];
    // present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
