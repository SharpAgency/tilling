//
//  BearersViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "BearersViewController.h"
#import "ModelWebViewController.h"
#import "BearersCalcViewController.h"

#define SINGLE_SPAN_IMAGE_NAME @"floorbearer_single"
#define CONTINUOUS_SPAN_IMAGE_NAME @"floorbearer_continuous"

@interface BearersViewController () {
    NSString *modelImageName;
    NSArray *results;
}

@end

@implementation BearersViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Below I have created separate methods to handle object creation
    // in order to keep ViewDidLoad looking clean.
    // This class also contains methods linked to IB for textField change events

    [self addDataToPickerViewArrays];
    [self createPickerViews];
    
    // After above init, set input for textfield to pickerviews
    [self.spanConditionTextField setInputView:_spanConditionPickerView];
    [self.liveLoadPointLoadTextField setInputView:_liveLoadPointLoadPickerView];
    [self.deadLoadTextField setInputView:_deadLoadPickerView];
    [self.loadWidthTextField setInputView:_loadWidthPickerView];
    [self.productTextField setInputView:_productPickerView];
    
    // Set accessoryViews
    [self.spanTextField setInputAccessoryView:_accessoryView];
    [self.spanConditionTextField setInputAccessoryView:_accessoryView];
    [self.liveLoadPointLoadTextField setInputAccessoryView:_accessoryView];
    [self.deadLoadTextField setInputAccessoryView:_accessoryView];
    [self.loadWidthTextField setInputAccessoryView:_accessoryView];
    [self.productTextField setInputAccessoryView:_accessoryView];
    
    // Set string defaults
    self.spanInt = 0;
    self.spanConditionString = @"Single Span";
    self.liveLoadPointLoadString = @"1.5kPa/1.8kN";
    self.deadLoadString = @"40";
    self.loadWidthString = @"1200";
    self.productString = @"SmartLVL15";
    
    modelImageName = SINGLE_SPAN_IMAGE_NAME;
}

#pragma mark - Table view data source
// CellForRow not needed when using static cells
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Check and activate appropriate textfields depending on cell selected
    if (indexPath.row == 0) [self.spanTextField becomeFirstResponder];
    if (indexPath.row == 1) [self.spanConditionTextField becomeFirstResponder];
    if (indexPath.row == 2) [self.deadLoadTextField becomeFirstResponder];
    if (indexPath.row == 3) [self.liveLoadPointLoadTextField becomeFirstResponder];
    if (indexPath.row == 4) [self.loadWidthTextField becomeFirstResponder];
    if (indexPath.row == 5) [self.productTextField becomeFirstResponder];
    if (indexPath.row == 6) [self calculate];
    if (indexPath.row == 7) {
        if ([_spanConditionString isEqualToString:@"Single Span"]) {
            modelImageName = SINGLE_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
        else if ([_spanConditionString isEqualToString:@"Continuous Span"]) {
            modelImageName = CONTINUOUS_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove all first responders.
    [self.view endEditing:YES];
}

#pragma mark - UIPickerView data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray count];
    if (pickerView == _liveLoadPointLoadPickerView) return [_liveLoadPointLoadArray count];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray count];
    if (pickerView == _loadWidthPickerView) return [_loadWidthArray count];
    if (pickerView == _productPickerView) return [_productArray count];
    return 0;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray objectAtIndex:row];
    if (pickerView == _liveLoadPointLoadPickerView) return [_liveLoadPointLoadArray objectAtIndex:row];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray objectAtIndex:row];
    if (pickerView == _loadWidthPickerView) return [_loadWidthArray objectAtIndex:row];
    if (pickerView == _productPickerView) return [_productArray objectAtIndex:row];
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // set textviews based on selection made in pickerview
    if (pickerView == _spanConditionPickerView) {
        self.spanConditionTextField.text = [_spanConditionArray objectAtIndex:row];
        self.spanConditionString = [_spanConditionArray objectAtIndex:row];
    }
    if (pickerView == _liveLoadPointLoadPickerView) {
        self.liveLoadPointLoadTextField.text = [_liveLoadPointLoadArray objectAtIndex:row];
        self.liveLoadPointLoadString = [_liveLoadPointLoadArray objectAtIndex:row];
    }
    if (pickerView == _deadLoadPickerView) {
        self.deadLoadTextField.text = [NSString stringWithFormat:@"%@kg/m2", [_deadLoadArray objectAtIndex:row]];
        self.deadLoadString = [_deadLoadArray objectAtIndex:row];
    }
    if (pickerView == _loadWidthPickerView) {
        self.loadWidthTextField.text = [NSString stringWithFormat:@"%@mm", [_loadWidthArray objectAtIndex:row]];
        self.loadWidthString = [_loadWidthArray objectAtIndex:row];
    }
    if (pickerView == _productPickerView) {
        self.productTextField.text = [_productArray objectAtIndex:row];
        self.productString = [_productArray objectAtIndex:row];
    }
}

#pragma mark - IB methods for textField changes
- (IBAction)spanEditingDidChange:(id)sender {
    self.spanInt = [_spanTextField.text intValue];
}

#pragma mark - init methods
- (void)createPickerViews {
    // Separate pickerviews for each occasion
    // makes it a little easier to make smaller custom changes and helps with debugging
    
    // Set up accessoryView. Used to dismiss pickerViews
    self.accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [self.accessoryView setItems:@[_doneButton]];
    
    // SpanCondtion PickerView
    self.spanConditionPickerView = [[UIPickerView alloc] init];
    self.spanConditionPickerView.delegate = self;
    self.spanConditionPickerView.dataSource = self;
    
    // LiveLoad PickerView
    self.liveLoadPointLoadPickerView = [[UIPickerView alloc] init];
    self.liveLoadPointLoadPickerView.delegate = self;
    self.liveLoadPointLoadPickerView.dataSource = self;
    
    // DeadLoad PickerView
    self.deadLoadPickerView = [[UIPickerView alloc] init];
    self.deadLoadPickerView.delegate = self;
    self.deadLoadPickerView.dataSource = self;
    
    // LoadWidth PickerView
    self.loadWidthPickerView = [[UIPickerView alloc] init];
    self.loadWidthPickerView.delegate = self;
    self.loadWidthPickerView.dataSource = self;
    
    // Product PickerView
    self.productPickerView = [[UIPickerView alloc] init];
    self.productPickerView.delegate = self;
    self.productPickerView.dataSource = self;
}

- (void)doneButtonPressed {
    [self.view endEditing:YES];
}

- (void)addDataToPickerViewArrays {
    self.spanConditionArray = @[@"Single Span",
                                @"Continuous Span"];
    
    self.liveLoadPointLoadArray = @[@"1.5kPa/1.8kN",
                                    @"2kPa/1.8kN",
                                    @"3kPa/1.7kN"];
   
    self.deadLoadArray =     @[@"40",
                               @"62",
                               @"100",
                               @"150",
                               @"200"];
    
    self.loadWidthArray = @[@"1200",
                            @"1800",
                            @"2400",
                            @"3000",
                            @"3600",
                            @"4200",
                            @"4800",
                            @"5400",
                            @"6000",
                            @"6600"];
    
    self.productArray = @[@"SmartLVL15",
                          @"SmartLam GL13c",
                          @"SmartLam GL17c",
                          @"SmartLam GL18c"];
}

- (void)calculate {
    // This Method will filter through a .plist database creating an array at the end depending on user inputs.
    
    // Load data to filter
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"floorBearerData" ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    // set span condition to integer to match with datasource value
    NSString *spanConditionNumber = @"0";
    if ([_spanConditionString isEqualToString:@"Single Span"]) spanConditionNumber = @"1";
    else if ([_spanConditionString isEqualToString:@"Continuous Span"]) spanConditionNumber = @"2";

    // set liveload to integer to match with datasource value
    NSString *liveLoadNumber = @"0";
    if ([_liveLoadPointLoadString isEqualToString:@"1.5kPa/1.8kN"]) liveLoadNumber = @"1";
    else if ([_liveLoadPointLoadString isEqualToString:@"2kPa/1.8kN"]) liveLoadNumber = @"2";
    else if ([_liveLoadPointLoadString isEqualToString:@"3kPa/1.7kN"]) liveLoadNumber = @"3";
    
    NSLog(@"%@", liveLoadNumber);

    // set product id to integer to match with datasource value
    NSString *productIDNumber = @"0";
    if ([_productString isEqualToString:@"SmartLVL15"]) productIDNumber = @"1";
    else if ([_productString isEqualToString:@"SmartLam GL13c"]) productIDNumber = @"3";
    else if ([_productString isEqualToString:@"SmartLam GL17c"]) productIDNumber = @"4";
    else if ([_productString isEqualToString:@"SmartLam GL18c"]) productIDNumber = @"5";

    // 1 Span Condition Filter
    NSPredicate *spanConditionPredicate = [NSPredicate predicateWithFormat:@"SELF.spanConditionID CONTAINS %@", spanConditionNumber];
    NSArray *filteredBySpanCondition = [NSArray arrayWithArray:[contentArray filteredArrayUsingPredicate:spanConditionPredicate]];
    
    // 2 Floor Live Load Filter
    NSPredicate *liveLoadPointLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.FllFplID CONTAINS %@", liveLoadNumber];
    NSArray *filteredbyLiveLoad = [NSArray arrayWithArray:[filteredBySpanCondition filteredArrayUsingPredicate:liveLoadPointLoadPredicate]];
    
    // 3 Dead Load Filter
    NSPredicate *deadLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.FloorDLMass CONTAINS %@", _deadLoadString];
    NSArray *filteredDeadLoad = [NSArray arrayWithArray:[filteredbyLiveLoad filteredArrayUsingPredicate:deadLoadPredicate]];
    
    // 4 Floor Load Width Filter
    NSPredicate *floorLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.floor_load_width CONTAINS %@", _loadWidthString];
    NSArray *filteredFloorLoad = [NSArray arrayWithArray:[filteredDeadLoad filteredArrayUsingPredicate:floorLoadPredicate]];
    
    // 5 product filter
    NSPredicate *productPredicate = [NSPredicate predicateWithFormat:@"SELF.productID CONTAINS %@", productIDNumber];
    NSArray *filteredProduct = [NSArray arrayWithArray:[filteredFloorLoad filteredArrayUsingPredicate:productPredicate]];
    
    // 6 SPan Filter
    NSPredicate *spanPredicate = [NSPredicate predicateWithFormat:@"SELF.span.intValue >= %d", [_spanTextField.text intValue]];
    NSArray *filteredSpan = [NSArray arrayWithArray:[filteredProduct filteredArrayUsingPredicate:spanPredicate]];
    
    results = filteredSpan;
    
    NSLog(@"Span Condition Count: %lu", (unsigned long)[filteredBySpanCondition count]);
    NSLog(@"Floor Live Load Count: %lu", (unsigned long)[filteredbyLiveLoad count]);
    NSLog(@"Dead Load Count: %lu", (unsigned long)[filteredDeadLoad count]);
    NSLog(@"Floor Width Count: %lu", (unsigned long)[filteredFloorLoad count]);
    NSLog(@"Product Count: %lu", (unsigned long)[filteredProduct count]);
    NSLog(@"Span Count: %lu", (unsigned long)[filteredSpan count]);
    
    [self performSegueWithIdentifier:@"resultsSegue" sender:self];
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"modelSegue"]) {
        ModelWebViewController *modelView = [segue destinationViewController];
        [modelView setImageName:modelImageName];
    }
    if ([[segue identifier] isEqualToString:@"resultsSegue"]) {
        BearersCalcViewController *vc = [segue destinationViewController];
        [vc setContent:results];
    }
}


@end
