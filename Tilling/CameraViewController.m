//
//  WallHeightViewController.m
//  Tilling
//
//  Created by Beau Young on 13/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "CameraViewController.h"
#import "TexturerSingletonHelper.h"

#define SOURCETYPE UIImagePickerControllerSourceTypeCamera

@interface CameraViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@end

@implementation CameraViewController {
    UIImage *chosenImage;
    UIImagePickerController *_picker;
}

- (void)viewDidLoad {
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.898 green:0.478 blue:0.000 alpha:1.000];
}

- (IBAction)openCamera:(UIButton *)sender {
    // Be sure to check if device has a camera before opening the camera
    if ([UIImagePickerController isSourceTypeAvailable:SOURCETYPE]) {
        UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 10)];
        UIImage *guideImage = [UIImage imageNamed:[[TexturerSingletonHelper sharedData] getNameForOverlayGuide]];
        UIImageView *guideImageView = [[UIImageView alloc] initWithImage:guideImage];
        guideImageView.userInteractionEnabled = NO;
        [overlayView addSubview:guideImageView];
        overlayView.userInteractionEnabled = NO;
        
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.allowsEditing = NO;
        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        _picker.cameraOverlayView = overlayView;
        [self presentViewController:_picker animated:YES completion:nil];
    }
}

- (IBAction)openPhotoAlbum:(UIButton *)sender {
    _picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = NO;
    _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:_picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[TexturerSingletonHelper sharedData] setPhoto:info[UIImagePickerControllerOriginalImage]];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self performSegueWithIdentifier:@"textureSegue" sender:self];
}

@end
