//
//  FSProductDetailsViewController.h
//  Tilling
//
//  Created by Beau Young on 6/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollImageView.h"

@interface FSProductDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet ScrollImageView *scrollingImageView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *arrayOfData;
@property (strong, nonatomic) NSArray *arrayOfTitles;
@property (strong, nonatomic) NSArray *colorArray;

@property (strong, nonatomic) NSMutableDictionary *data;
@property (strong, nonatomic) NSString *imagePath;


@end
