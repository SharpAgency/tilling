//
//  FSProfilesTableViewController.h
//  Tilling
//
//  Created by Beau Young on 6/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSProfilesTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *images;
@property (strong, nonatomic) NSArray *titles;
@property (strong, nonatomic) NSArray *products;

@end
