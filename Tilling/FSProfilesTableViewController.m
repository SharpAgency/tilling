//
//  FSProfilesTableViewController.m
//  Tilling
//
//  Created by Beau Young on 6/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "FSProfilesTableViewController.h"
#import "ArchitecturalTableViewCell.h"
#import "FSProductDetailsViewController.h"

@interface FSProfilesTableViewController ()

@end

@implementation FSProfilesTableViewController {
    NSIndexPath *savedIndexpath;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.products = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"frameSmartProducts" ofType:@"plist"]];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:90/255.0 green:111/255.0 blue:186/255.0 alpha:1.000];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.products count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set Cell identifier
    static NSString *CellIdentifier = @"Cell";
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // If no cell, create one with custom nib
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    cell.titleLabel.text = [[self.products objectAtIndex:indexPath.row] valueForKey:@"product"];
    cell.detailLabel.text = nil;
    
    cell.noShadow = YES;
    
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.148 green:0.268 blue:0.448 alpha:0.380];
    
    if (indexPath.row % 2) {
        cell.backgroundImage.backgroundColor = [UIColor colorWithRed:90/255.0 green:111/255.0 blue:186/255.0 alpha:0.600];
    }else {
        cell.backgroundImage.backgroundColor = [UIColor colorWithRed:90/255.0 green:111/255.0 blue:186/255.0 alpha:0.600];
    }
    
    return cell;}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    savedIndexpath = [[NSIndexPath alloc] init];
    savedIndexpath = indexPath;
    
    [self performSegueWithIdentifier:@"detailsSegue" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    FSProductDetailsViewController *vc = [segue destinationViewController];
    [vc setData:[self.products objectAtIndex:savedIndexpath.row]];
}


@end
