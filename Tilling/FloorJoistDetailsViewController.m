//
//  FloorJoistDetailsViewController.m
//  Tilling
//
//  Created by Beau Young on 24/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "FloorJoistDetailsViewController.h"
#import <MessageUI/MessageUI.h>


@interface FloorJoistDetailsViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation FloorJoistDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Setup product strings
    if ([[_content objectForKey:@"productID"] isEqualToString:@"2"]) self.productLabel.text = @"SmartJoist";
    else if ([[_content objectForKey:@"productID"] isEqualToString:@"1"]) self.productLabel.text = @"SmartLvl15";
    
    self.sizeLabel.text = [_content objectForKey:@"DxB"];
    
    // setup spanCondition strings
    if ([[_content objectForKey:@"spanConditionID"] isEqualToString:@"1"]) self.spanConditionLabel.text = @"Single Span";
    else if ([[_content objectForKey:@"spanConditionID"] isEqualToString:@"2"]) self.spanConditionLabel.text = @"Continuous Span";
    
    self.joistSpacingLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"joistSpacing"]];
    self.spanLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"span"]];
    
    // Ceiling attached string
    if ([[_content objectForKey:@"ceilingAttachedID"] isEqualToString:@"1"]) self.ceilingLabel.text = @"NO";
    else if ([[_content objectForKey:@"ceilingAttachedID"] isEqualToString:@"2"]) self.ceilingLabel.text = @"YES";
    
    // dead load
    self.deadLoadLabel.text = [NSString stringWithFormat:@"%@kg/m2", [_content objectForKey:@"floorDL"]];
    
    // live load
    if ([[_content objectForKey:@"fllFplID"] isEqualToString:@"1"]) self.liveLoadLabel.text = @"1.5kPa/1.8kN";
    else if ([[_content objectForKey:@"fllFplID"] isEqualToString:@"2"]) self.liveLoadLabel.text = @"2kPa/1.8kN";
    else if ([[_content objectForKey:@"fllFplID"] isEqualToString:@"3"]) self.liveLoadLabel.text = @"3kPa/2.7kN";
    
    self.endBearingLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"EB"]];
    if (![[_content objectForKey:@"IB"] isEqualToString:@""]) {
        self.internalBearingLabel.text = [NSString stringWithFormat:@"%@mm", [_content objectForKey:@"IB"]];
    }
    else self.internalBearingLabel.text = @"0mm";
    
}
- (IBAction)emailBtn:(UIBarButtonItem *)sender {
    // email subject
    NSString *emailSubject = @"Tilling Building Calculator - Your Results";
    NSString *messageBody = [NSString stringWithFormat:@"Product: %@\n Size DxB (mm): %@\n Span Condition: %@\n Joist Spacing: %@\n Span: %@\n Ceiling Attatched: %@\n Floor Dead Load: %@\n Floor Live Load/Floor Point Load: %@\n End Bearing Length Required: %@\n Internal Bearing Length Required: %@\n",
                             _productLabel.text,
                             _sizeLabel.text,
                             _spanConditionLabel.text,
                             _joistSpacingLabel.text,
                             _spanLabel.text,
                             _ceilingLabel.text,
                             _deadLoadLabel.text,
                             _liveLoadLabel.text,
                             _endBearingLabel.text,
                             _internalBearingLabel.text];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailSubject];
    [mc setMessageBody:messageBody isHTML:NO];
    // present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
