//
//  FloorJoistsViewController.h
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldWithNoCaret.h"

@interface FloorJoistsViewController : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate>
// Each property is declared in order of appearance.
// textField
@property (weak, nonatomic) IBOutlet UITextField *spanTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *spanConditionTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *liveLoadPointLoadTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *deadLoadTextField;
@property (weak, nonatomic) IBOutlet UISwitch *ceilingAttachedSwitch;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *joistSpacingTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *productTextField;

// For storing TextField Values
@property (nonatomic) int spanInt;
@property (weak, nonatomic) NSString *spanConditionString;
@property (weak, nonatomic) NSString *liveLoadPointLoadString;
@property (weak, nonatomic) NSString *deadLoadString;
@property (weak, nonatomic) NSString *ceilingAttachedString;
@property (weak, nonatomic) NSString *joistSpacingString;
@property (weak, nonatomic) NSString *productString;

// UIPickerViews
@property (strong, nonatomic) UIPickerView *spanConditionPickerView;
@property (strong, nonatomic) UIPickerView *liveLoadPointLoadPickerView;
@property (strong, nonatomic) UIPickerView *deadLoadPickerView;
@property (strong, nonatomic) UIPickerView *joistSpacingPickerView;
@property (strong, nonatomic) UIPickerView *productPickerView;

// UIPickerView ToolBar - Done Button.
@property (strong, nonatomic) UIToolbar *accessoryView;
@property (strong, nonatomic) UIBarButtonItem *doneButton;

// UIPickerView data arrays
@property (strong, nonatomic) NSArray *spanConditionArray;
@property (strong, nonatomic) NSArray *liveLoadPointLoadArray;
@property (strong, nonatomic) NSArray *deadLoadArray;
@property (strong, nonatomic) NSArray *joistSpacingArray;
@property (strong, nonatomic) NSArray *productArray;

@end
