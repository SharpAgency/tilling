//
//  FrameSmartMenuViewController.m
//  Tilling
//
//  Created by Beau Young on 29/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "FrameSmartMenuViewController.h"
#import "ArchitecturalTableViewCell.h"
#import "Termite.h"

@implementation FrameSmartMenuViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellBackgroundColorArray;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.353 green:0.435 blue:0.729 alpha:1.000];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    cellTitles = @[@"FrameSmart",
                   @"Products",
                   @"SmartGuard",
                   @"Product Profiles",
                   @"Visit this sections website"];
    
    cellBackgroundColorArray = @[[UIColor colorWithRed:0.215 green:0.250 blue:0.666 alpha:0.600],
                                 [UIColor colorWithRed:0.255 green:0.290 blue:0.706 alpha:0.600],
                                 [UIColor colorWithRed:0.295 green:0.330 blue:0.746 alpha:0.600],
                                 [UIColor colorWithRed:0.335 green:0.370 blue:0.786 alpha:0.600],
                                 [UIColor colorWithRed:0.375 green:0.410 blue:0.826 alpha:0.600],
                                 ];
    
    cellDetailText = @[@"Pine products are graded in various ways, depending upon their end usage. Broadly speaking, there are two types of pine products:\n\n1. Appearance Products. These are products used in applications where visual characteristics are most important e.g. lining boards, cladding, flooring, etc. Including our comprehensive range of exterior, pre-primed architecturally inspired profiles called SmartPrime.\n\n2. Structural Products. These products are used in structural applications where strength and stiffness characteristics are most important e.g. house framing.\n\nIn some cases a particular product may need to meet appearance as well as structural criteria, e.g. exposed beams or flooring.\n\nThere are two common methods of grading currently used in Australia: 1.\n\n Visual Grading involves the manual examination of a piece of timber and its assessment against a set of criteria. There are limits on characteristics such as knot size, slope of grain etc. Visual grading can be used for appearance as well as structural timber.\n\n2. Mechanical Stress Grading is used for grading structural timber. The timber is fed through a machine that applies a load through a roller. The stiffness of the piece is determined based on its detection and a grade assigned. Mechanical grading is used for the majority of structural pine produced in Australia.",
                       
                       @"",
                       @"",
                       @"",
                       @""];
    
    // Add termites to tableview
    Termite *termiteOne = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(22, 45) isHorizontal:YES];
    [self.tableView addSubview:termiteOne];
    [termiteOne faceRight:nil finished:nil context:nil];
    
    Termite *termiteTwo = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(100, 470) isHorizontal:YES];
    [self.tableView addSubview:termiteTwo];
    [termiteTwo faceLeft:nil finished:nil context:nil];
    
    Termite *termiteThree = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(80, 520) isHorizontal:YES];
    [self.tableView addSubview:termiteThree];
    [termiteThree faceUp:nil finished:nil context:nil];
    
    Termite *termiteFour = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(460, 200) isHorizontal:YES];
    [self.tableView addSubview:termiteFour];
    [termiteFour faceLeft:nil finished:nil context:nil];
    
    Termite *termiteFive = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(300, 360) isHorizontal:YES];
    [self.tableView addSubview:termiteFive];
    [termiteFive faceDown:nil finished:nil context:nil];
    
}

#pragma mark - TableView delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.section];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.section];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [cellBackgroundColorArray objectAtIndex:indexPath.section];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
    
    
    if (indexPath.section == 1) [self performSegueWithIdentifier:@"productsSegue" sender:nil];
    if (indexPath.section == 2) [self performSegueWithIdentifier:@"smartGuardSegue" sender:nil];
    if (indexPath.section == 3) [self performSegueWithIdentifier:@"profilesSegue" sender:nil];
    if (indexPath.section == 4) [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tilling.com.au/framesmart"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 44 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.section == 0) return 750; // Expanded height
    }
    return 56;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 168;
    }
    return 0; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
