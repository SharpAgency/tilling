//
//  FrameSmartProductsViewController.h
//  Tilling
//
//  Created by Beau Young on 16/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrameSmartProductsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
