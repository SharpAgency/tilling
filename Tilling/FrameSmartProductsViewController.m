//
//  FrameSmartProductsViewController.m
//  Tilling
//
//  Created by Beau Young on 16/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "FrameSmartProductsViewController.h"
#import "ArchitecturalTableViewCell.h"
#import "Termite.h"

@interface FrameSmartProductsViewController ()

@end

@implementation FrameSmartProductsViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellTitles;
    NSArray *cellDetailText;
    NSArray *colorArray;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    colorArray = @[[UIColor colorWithRed:0.215 green:0.250 blue:0.666 alpha:0.600],
                   [UIColor colorWithRed:0.255 green:0.290 blue:0.706 alpha:0.600],
                   [UIColor colorWithRed:0.295 green:0.330 blue:0.746 alpha:0.600],
                   [UIColor colorWithRed:0.335 green:0.370 blue:0.786 alpha:0.600]];
    
    cellTitles = @[@"KD Canadian Lodgepole Pine (LpP)",
                   @"KD Doug Fir (Oregon)",
                   @"Red Baltic Pine",
                   @"White Baltic Spruce"];
    
    
    cellDetailText = @[@"FrameSmart Canadian LpP framing products are produced to comply with AS/NZS 1748, further, the material properties conform to AS1720.1 (1997 & 2010). FrameSmart Canadian LpP is available in MGP 10 and F7 Grades.\n\nPine products are graded in various ways, depending upon their end usage. Broadly speaking, there are two types of pine products:\n\n1. Appearance Products. These are products used in applications where visual characteristics are most important e.g. Lining boards, cladding, flooring, etc.\n\n2. Structural Products. These products are used in structural applications where strength and stiffness characteristics are most important e.g. house framing.\n\nIn some cases a particular product may need to meet appearance as well as structural criteria, e.g. exposed beams or flooring.\n\nThere are three methods of grading currently used in Australia: 1. Visual grading involves the manual examination of a piece of timber and its assessment against a set of criteria. There are limits on characteristics such as knot size, slope of grain etc. Visual grading can be used for appearance as well as structural timber.\n\n2. Mechanical Stress Grading is used for grading structural timber. The timber is fed through a machine that applies a load through a roller. The stiffness of the piece is determined based on its deflection, and a grade assigned. Mechanical grading is used for the majority of structural pine produced in Australia.\n\n3. Mechanical Proof Grading also uses a machine to apply a load to the piece of timber as it passes through. In this case, however, the timber is loaded to a predetermined proof load. A piece of timber is accepted as belonging to a specific grade if it passes without breaking. Proof grading is used for structural timber only. A small volume of timber is graded in this manner.\n\nReferences: Pine Australia Brochure: Pine Possibilities – An Introduction to Pine.",
                       
                       
                       @"FrameSmart Doug Fir (Oregon) framing products are produced to comply with AS/NZS 1748, further, the material properties conform to AS1720.1 (1997 & 2010). FrameSmart Doug Fir (Oregon) is available in MGP 10 Grade.\n\nFraming products are graded in various ways depending upon their end usage. Broadly speaking, there are two types of pine products:\n\n1. Appearance Products. These are products used in applications where visual characteristics are most important e.g. Lining boards, cladding, flooring, etc.\n\n2. Structural Products. These products are used in structural applications where strength and stiffness characteristics are most important e.g. house framing.\n\nIn some cases a particular product may need to meet appearance as well as structural criteria, e.g. exposed beams or flooring.\n\nThere are three methods of grading currently used in Australia: 1. Visual grading involves the manual examination of a piece of timber and its assessment against a set of criteria. There are limits on characteristics such as knot size, slope of grain etc. Visual grading can be used for appearance as well as structural timber.\n\n2. Mechanical Stress Grading is used for grading structural timber. The timber is fed through a machine that applies a load through a roller. The stiffness of the piece is determined based on its deflection, and a grade assigned. Mechanical grading is used for the majority of structural pine produced in Australia.\n\n3. Mechanical Proof Grading also uses a machine to apply a load to the piece of timber as it passes through. In this case, however, the timber is loaded to a predetermined proof load. A piece of timber is accepted as belonging to a specific grade if it passes without breaking. Proof grading is used for structural timber only. A small volume of timber is graded in this manner.\n\nReferences: Pine Australia Brochure: Pine Possibilities – An Introduction to Pine.",
                       
                       @"FrameSmart Red Baltic Pine framing products are produced to comply with AS/NZS 1748, further, the material properties conform to AS1720.1 (1997 & 2010). FrameSmart Red Baltic Pine is available in MGP 10, MGP 12 and F5 Grades.\n\nPine products are graded in various ways depending upon their end usage. Broadly speaking, there are two types of pine products:\n\n1. Appearance Products. These are products used in applications where visual characteristics are most important e.g. Lining boards, cladding, flooring, etc.\n\n2. Structural Products. These products are used in structural applications where strength and stiffness characteristics are most important e.g. house framing.\n\nIn some cases a particular product may need to meet appearance as well as structural criteria, e.g. exposed beams or flooring.\n\nThere are three methods of grading currently used in Australia: 1. Visual grading involves the manual examination of a piece of timber and its assessment against a set of criteria. There are limits on characteristics such as knot size, slope of grain etc. Visual grading can be used for appearance as well as structural timber.\n\n2. Mechanical Stress Grading is used for grading structural timber. The timber is fed through a machine that applies a load through a roller. The stiffness of the piece is determined based on its deflection, and a grade assigned. Mechanical grading is used for the majority of structural pine produced in Australia.\n\n3. Mechanical Proof Grading also uses a machine to apply a load to the piece of timber as it passes through. In this case, however, the timber is loaded to a predetermined proof load. A piece of timber is accepted as belonging to a specific grade if it passes without breaking. Proof grading is used for structural timber only. A small volume of timber is graded in this manner.\n\nReferences: Pine Australia Brochure: Pine Possibilities – An Introduction to Pine.",
                       
                       @"FrameSmart White Baltic Spruce framing products are produced to comply with AS/NZS 1748, further, the material properties conform to AS1720.1 (1997 & 2010). FrameSmart White Baltic Spruce is available in MGP 10, MGP 12 and F5 Grades.\n\nPine products are graded in various ways depending upon their end usage. Broadly speaking, there are two types of pine products:\n\n1. Appearance Products. These are products used in applications where visual characteristics are most important e.g. Lining boards, cladding, flooring, etc.\n\n2. Structural Products. These products are used in structural applications where strength and stiffness characteristics are most important e.g. house framing.\n\nIn some cases a particular product may need to meet appearance as well as structural criteria, e.g. exposed beams or flooring.\n\nThere are three methods of grading currently used in Australia: 1. Visual grading involves the manual examination of a piece of timber and its assessment against a set of criteria There are limits on characteristics such as knot size, slope of grain etc. Visual grading can be used for appearance as well as structural timber.\n\n2. Mechanical Stress Grading is used for grading structural timber. The timber is fed through a machine that applies a load through a roller. The stiffness of the piece is determined based on its deflection, and a grade assigned. Mechanical grading is used for the majority of structural pine produced in Australia.\n\n3. Mechanical Proof Grading also uses a machine to apply a load to the piece of timber as it passes through. In this case, however, the timber is loaded to a predetermined proof load. A piece of timber is accepted as belonging to a specific grade if it passes without breaking. Proof grading is used for structural timber only. A small volume of timber is graded in this manner.\n\nReferences: Pine Australia Brochure: Pine Possibilities – An Introduction to Pine."];
    
    
    
    // Add termites to scrollview
    Termite *termiteOne = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(22, 45) isHorizontal:YES];
    [self.tableView addSubview:termiteOne];
    [termiteOne faceRight:nil finished:nil context:nil];
    
    Termite *termiteTwo = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(100, 470) isHorizontal:YES];
    [self.tableView addSubview:termiteTwo];
    [termiteTwo faceLeft:nil finished:nil context:nil];
    
    Termite *termiteThree = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(80, 520) isHorizontal:YES];
    [self.tableView addSubview:termiteThree];
    [termiteThree faceUp:nil finished:nil context:nil];
    
    
    Termite *termiteFour = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(460, 200) isHorizontal:YES];
    [self.tableView addSubview:termiteFour];
    [termiteFour faceLeft:nil finished:nil context:nil];
    
    Termite *termiteFive = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(300, 360) isHorizontal:YES];
    [self.tableView addSubview:termiteFive];
    [termiteFive faceDown:nil finished:nil context:nil];
}

#pragma mark - Tableview datasource and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.row];
    cell.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.row];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.148 green:0.268 blue:0.448 alpha:0.380];
    
    // Clear backgroundColor for transparency
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundImage.backgroundColor = colorArray[indexPath.row];

    
    // remove shadows
    cell.noShadow = YES;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 56 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        // Expanded height
        if (indexPath.row == 0) return 950; // Canadian LP
        if (indexPath.row == 1) return 950; // Doug Fir
        if (indexPath.row == 2) return 950; // Baltic Pine
        if (indexPath.row == 3) return 950; // Baltic spruce
    }
    return 56;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove shadows
    [[cell layer] setShadowOpacity:0.0];
    [[cell layer] setShadowRadius:0.0];
    [[cell layer] setShadowColor:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
