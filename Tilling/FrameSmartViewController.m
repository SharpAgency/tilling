//
//  FrameSmartViewController.m
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "FrameSmartViewController.h"
#import "Termite.h"

@interface FrameSmartViewController () {
    bool bugDead;

}

@end

@implementation FrameSmartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view
    [_firstTextView setText:@"Unfortunately Australia’s termite problem continues to grow from year to year. Termites are a major contributor to the degradation of structural support materials in our homes. Including the European House Borer (EHB) which is a serious pest of untreated dry softwood, including pinewood, particularly in Western Australia. \n\n Without appropriate action, EHB has the potential to infest homes built with untreated pinewood structural timber. FrameSmart pine framing products have the option of being treated with SmartGuard. SmartGuard termite treatments assist in protecting your building, speak to our friendly customer service team to find out more regarding treatment options, call 1300 668 690."];
    
    [_secondTextView setText:@"Tilling provides a fully transferable ‘25 year guarantee’ on all SmartGuard treated pine framing products. This provides you with the reassurance that the product you purchase shall withstand an attack from Termites and European House Borer for a period of 25 years from the date of supply. Terms and conditions apply and are available upon request via our technical support line, 1300 668 690."];
    
//    self.firstTextView.textAlignment = NSTextAlignmentJustified;
    self.firstTextView.textColor = [UIColor whiteColor];
    self.firstTextView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    
//    self.secondTextView.textAlignment = NSTextAlignmentJustified;
    self.secondTextView.textColor = [UIColor whiteColor];
    self.secondTextView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
}

- (void)viewDidAppear:(BOOL)animated {
    
    // Load images
    NSArray *imageNames = @[@"termiteOne", @"termiteTwo"];
    
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }

    // Add termites to scrollview
    Termite *termiteOne = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(22, 45) isHorizontal:YES];
    [self.scrollView addSubview:termiteOne];
    [termiteOne faceRight:nil finished:nil context:nil];
    
    Termite *termiteTwo = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(100, 470) isHorizontal:YES];
    [self.scrollView addSubview:termiteTwo];
    [termiteTwo faceLeft:nil finished:nil context:nil];
    
    Termite *termiteThree = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(80, 520) isHorizontal:YES];
    [self.scrollView addSubview:termiteThree];
    [termiteThree faceUp:nil finished:nil context:nil];
    
    
    Termite *termiteFour = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(460, 200) isHorizontal:YES];
    [self.scrollView addSubview:termiteFour];
    [termiteFour faceLeft:nil finished:nil context:nil];
    
    Termite *termiteFive = [[Termite alloc] initWithTermiteAtLocation:CGPointMake(300, 360) isHorizontal:YES];
    [self.scrollView addSubview:termiteFive];
    [termiteFive faceDown:nil finished:nil context:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






@end
