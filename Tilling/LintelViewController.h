//
//  LintelViewController.h
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldWithNoCaret.h"

@interface LintelViewController : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate>

// Each property is declared in order of appearance.
@property (weak, nonatomic) IBOutlet UITextField *spanTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *spanConditionTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *deadLoadTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *loadWidthTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *rafterSpacingTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *windSpeedTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *productTextField;

// For storing TextField Values
@property (nonatomic) int spanInt;
@property (weak, nonatomic) NSString *spanConditionString;
@property (weak, nonatomic) NSString *deadLoadString;
@property (weak, nonatomic) NSString *loadWidthString;
@property (weak, nonatomic) NSString *rafterSpacingString;
@property (weak, nonatomic) NSString *windSpeedString;
@property (weak, nonatomic) NSString *productString;

// UIPickerViews for each textField
@property (strong, nonatomic) UIPickerView *spanConditionPickerView;
@property (strong, nonatomic) UIPickerView *deadLoadPickerView;
@property (strong, nonatomic) UIPickerView *loadWidthPickerView;
@property (strong, nonatomic) UIPickerView *rafterSpacingPickerView;
@property (strong, nonatomic) UIPickerView *windSpeedPickerView;
@property (strong, nonatomic) UIPickerView *productPickerView;

// UIPickerView ToolBar - Done Button.
@property (strong, nonatomic) UIToolbar *accessoryView;
@property (strong, nonatomic) UIBarButtonItem *doneButton;

// UIPickerView data arrays
@property (strong, nonatomic) NSArray *spanConditionArray;
@property (strong, nonatomic) NSArray *deadLoadArray;
@property (strong, nonatomic) NSArray *loadWidthArray;
@property (strong, nonatomic) NSArray *rafterSpacingArray;
@property (strong, nonatomic) NSArray *windSpeedArray;
@property (strong, nonatomic) NSArray *productArray;

@end
