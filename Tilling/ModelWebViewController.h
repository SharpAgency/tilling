//
//  ModelWebViewController.h
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModelWebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) NSString *imageName;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end
