//
//  ModelWebViewController.m
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ModelWebViewController.h"

@interface ModelWebViewController ()

@end

@implementation ModelWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.webView.scalesPageToFit = YES;
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle bundlePath];
    NSString *fullPath = [NSBundle pathForResource:_imageName ofType:@"jpg" inDirectory:path];
    NSURL *filePathURL = [NSURL fileURLWithPath:fullPath];
    [self.webView loadRequest:[NSURLRequest requestWithURL:filePathURL]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
