//
//  ProductDetailsViewController.m
//  Tilling
//
//  Created by Maris on 10/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ProductDetailsViewController.h"

@implementation ProductDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // be sure to set the delegate and datasource for the tableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"yellowGrid"]];
    
    self.identifierArray = @[@"profile",
                             @"species",
                             @"exterior",
                             @"finish",
                             @"size",
                             @"bundleSize",
                             @"code"];
    
    // Create array for cell titles.
    self.arrayOfTitles = @[@"Profile",
                           @"Species",
                           @"Exterior/Interior",
                           @"Finish/Colour",
                           @"Size",
                           @"Bundle Size",
                           @"Code"];
    
    // Create array for Cell Data
    self.arrayOfData = @[[_data objectForKey:@"profile"], // Must be in same order as above array and reflect PLIST
                         [_data objectForKey:@"Species"],
                         [_data objectForKey:@"exterior/interior"],
                         [_data objectForKey:@"finish/color"],
                         [_data objectForKey:@"size"],
                         [_data objectForKey:@"bundle"],
                         [_data objectForKey:@"code"]];
    
    // create array for cell backgrund colors
    self.colorArray = @[[UIColor colorWithHue:0.093 saturation:0.150 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.093 saturation:0.200 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.093 saturation:0.250 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.093 saturation:0.300 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.093 saturation:0.350 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.093 saturation:0.400 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.093 saturation:0.450 brightness:0.949 alpha:1.000]];
    
	// Do any additional setup after loading the view.
    self.scrollingImageView = [[ScrollImageView alloc] initWithFrame:self.scrollingImageView.frame andImagePath:[_data objectForKey:@"imageArray"]];
    
    ScrollImageView *imageAnimation = [[ScrollImageView alloc] initWithFrame:self.scrollingImageView.frame andImagePath:[_data objectForKey:@"imageArray"]];
    
    [self.view addSubview:imageAnimation];
}

#pragma mark - TableView Datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set Cell identifier
    NSString *CellIdentifier = [_identifierArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // configure the cell
    cell.textLabel.text = [_arrayOfTitles objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [_arrayOfData objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    cell.backgroundColor = [_colorArray objectAtIndex:indexPath.row];
    
    
}

@end
