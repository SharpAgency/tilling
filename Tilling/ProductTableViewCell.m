//
//  ProductTableViewCell.m
//  Tilling
//
//  Created by Maris on 9/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ProductTableViewCell.h"

@implementation ProductTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.gridImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"yellowGrid"]];
    
    // Add shadows to tableview cells
//    self.layer.shadowOffset = CGSizeMake(1, 0);
//    self.layer.shadowColor = [[UIColor blackColor] CGColor];
//    self.layer.shadowRadius = 10;
//    self.layer.shadowOpacity = .3;
//    
//    CGRect shadowFrame = self.layer.bounds;
//    CGPathRef shadowPath = [UIBezierPath bezierPathWithRect:shadowFrame].CGPath;
//    self.layer.shadowPath = shadowPath;

}

@end
