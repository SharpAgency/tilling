//
//  ProductsViewController.m
//  Tilling
//
//  Created by Beau Young on 2/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ProductsViewController.h"
#import "ArchitecturalTableViewCell.h"

@implementation ProductsViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.146 green:0.682 blue:0.759 alpha:0.800];
    
    cellTitles = @[@"SmartEdge",
                   @"SmartForm",
                   @"SmartJoist",
                   @"SmartLam GL13",
                   @"SmartLam GL17",
                   @"SmartLam GL18",
                   @"SmartLVL 15",
                   @"SmartLVL 18",
                   @"SmartLVL FR",
                   @"SmartPlank",
                   @"TecBeam",
                   @"TecSlab"];
    
    cellDetailText = @[@"SmartEdge is a lightweight laminated veneer lumber, engineered wood product utilised in concrete construction applications.\n\nThe cross lamination of the layered veneers provides a protective mechanism, preventing an active capillary effect on the inside wall reducing the effects of cupping.\n\nProduced from sustainable timber, and harvested from certified plantations, SmartEdge is not only a straight and true, uniformly consistant edging, it is also an environmentally friendly composite timber product.",
                       
                       @"SmartForm 11.7 LVL is a Douglas Fir structural laminated veneer lumber (LVL), manufactured by Pacific Woodtech Corporation Washington State USA, to meet the requirements of AS/NZS 4357 - Structural Laminated Veneer Lumber. It is produced in industry standard sizes, painted safety yellow for easy visual identification, and is intended for use as concrete formwork support structures such as joists, bearers, walers and soldiers.\n\nSmartForm 11.7 is manufactured for Tilling Timber Pty Ltd under an evergreen exclusive agreement with the worlds best toll LVL manufacturer under the benchmark quality systems of the APA - The Engineered Wood Association.",
                       
                       
                       @"Adding to our already comprehensive range of I-Joist products, the new 70mm SmartJoist provides end users with a perfect solution for those projects that fall between the 51mm and 90mm, traditional, I-Joist applications.\n\nThe Strength is in the engineering - Strong. Stiff. Reliable. Smartjoists are engineered for heavy performance. We start with ultrasonically graded LVL flanges, bonded with exterior adhesive for more load carrying capacity. The web material is O.S.B (orientated strand board) which also has superior carrying capacity.",
                       
                       @"SmartLam GL13 beams are manufactured to AS/NZS 1328 by quality Glulam manufacturers.\n\nSmartLam GL13 Glulam beams are engineered timber products with high strength, dimensional stability, great load carrying capacity, superior fire resistance, and are manufactured from select quality Pine timber.\n\nAll timber used for laminating is carefully selected from production and graded according to specification. After trimming to the desired size, all stock is kiln dried to 12% average moisture content, to ensure efficient bonding in the gluing operations.",
                       
                       @"SmartLam GL17 beams are manufactured to AS/NZS 1328 by quality Glulam manufacturers.\n\nSmartLam GL17 Glulam beams are engineered timber products with high strength, dimensional stability, great load carrying capacity, superior fire resistance, and are manufactured from select quality Pine timber.\n\nAll timber used for laminating is carefully selected from production and graded according to specification. After trimming to the desired size, all stock is kiln dried to 12% average moisture content, to ensure efficient bonding in the gluing operations.",
                       
                       @"SmartLam GL18 beams are manufactured to AS/NZS 1328 by quality Glulam manufacturers.\n\nSmartLam GL18 Glulam beams are engineered timber products with high strength, dimensional stability, great load carrying capacity, superior fire resistance, and are manufactured from select quality Australian Hardwood.\n\nAll timber used for laminating is carefully selected from production and graded according to specification. After trimming to the desired size, all stock is kiln dried to 12% average moisture content, to ensure efficient bonding in the gluing operations.",
                       
                       @"SmartLVL 15 delivers the next generation in Laminated Veneer Lumber (LVL). SmartLVL 15 allows smaller cross sections for the same properties as some common LVL.\n\nManufactured for Douglas Fir, SmartLVL 15 is naturally engineered for strength.\n\nPlease be advised the new SmartLVL 15 Design guide has been released. Please note that this Design Guide is sutiable for all parts of Australia, except Western Australia. Please see additional Design Guide for Western Australia below.",
                       
                       @"SmartFrame LVL 18 is dimensionally stable and resists warping and twisting and is machined to consistantly uniform sizes. Natural defects are dispersed throughout the member, unlike solid timber with knots. This uniformity provides a rigid, flat surface with good nail holding characteristics. It's a high strength structural member you can cut, fasten and nail with ease.",
                       
                       @"Many building materials except timber are likely to show significant loss of strength when heated above 250 ºC, strength that may not recover after cooling. 1. There are also the well-known colour changes in concrete or mortar. The development of red or pink colouration in concrete or mortar containing natural sands or aggregates of appreciable iron oxide content occurs at 250 to 300 oC and normally, 300 oC may be taken as the transition temperature.",
                       
                       @"SmartPlank is a lightweight yet robust structural Laminated Veneer lumber (LVL) manufactured to AS/NZS 4357, sized and engineered to meet or exceed the performance requirements for scaffold planks as defined in AS 1577 Scaffold Planks.",
                       
                       @"Tecbeam Australasia is proud Australian building products innovator, licensor and manufacturer. Its core technology is the TECBEAM® ‘I’ Beam/Joist, a unique, patent protected, innovative light weight steel and timber composite structural beam.\n\nThe composite TECBEAM® ‘I’ Beam/Joist comprises a continuous light gauge galvanized steel web, with press formed stiffening ribs and uniformly spaced service holes, and structural timber flanges of plantation-grown softwood, fixed by nails and/or spikes to each side of the continuous steel web. ",
                       
                       @"The affordable Premium lightweight timber framed floor system that feels like a concrete floor.\n\nTECSLAB™ is designed for low, medium and high-density construction. The TECSLAB™ system combines the properties of two exceptional construction products TECBEAM™ and Hebel® PowerFloor™ to provide a superior floor solution achieving quicker build times and significant cost savings.\n\nHebel® is a lightweight steel-reinforced Autoclaved Aerated Concrete (AAC) that has been used in Europe for over 70 years and here in Australia for over 20 years. "];
}

#pragma mark - TableView delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.row];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.row];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0.148 green:0.268 blue:0.448 alpha:0.380];

    // Clear backgroundColor for transparency
    cell.backgroundColor = [UIColor clearColor];
    
    // remove shadows
    cell.noShadow = YES;
    
    // check if row is odd or even and set color accordingly
    if (indexPath.row % 2) {
        cell.backgroundImage.backgroundColor = [UIColor colorWithRed:0.438 green:0.903 blue:0.998 alpha:0.600];
    }else {
        cell.backgroundImage.backgroundColor = [UIColor colorWithRed:0.086 green:0.685 blue:0.848 alpha:0.600];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 56 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        // Expanded height
        if (indexPath.row == 0) return 320; // SmartEdge
        if (indexPath.row == 1) return 360; // SmartForm
        if (indexPath.row == 2) return 300; // SmartJoist
        if (indexPath.row == 3) return 345; // SmartLam GL13
        if (indexPath.row == 4) return 340; // SmartLam GL17
        if (indexPath.row == 5) return 340; // SmartLam GL18
        if (indexPath.row == 6) return 320; // SmartLVL 15
        if (indexPath.row == 7) return 220; // SmartLVL 18
        if (indexPath.row == 8) return 250; // SmartLVL FR
        if (indexPath.row == 9) return 175; // SmartPlank
        if (indexPath.row == 10) return 320; // TecBeam
        if (indexPath.row == 11) return 345; // TecSlab
    }
    return 56;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove shadows
    [[cell layer] setShadowOpacity:0.0];
    [[cell layer] setShadowRadius:0.0];
    [[cell layer] setShadowColor:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
