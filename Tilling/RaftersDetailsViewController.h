//
//  RaftersDetailsViewController.h
//  Tilling
//
//  Created by Beau Young on 24/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaftersDetailsViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary *content;
@property (strong, nonatomic) NSString *windSpeed;

@property (strong, nonatomic) IBOutlet UILabel *productLabel;
@property (strong, nonatomic) IBOutlet UILabel *sizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *spanConditionLabel;
@property (strong, nonatomic) IBOutlet UILabel *rafterSpacingLabel;
@property (strong, nonatomic) IBOutlet UILabel *spanLabel;
@property (strong, nonatomic) IBOutlet UILabel *overhangLabel;
@property (strong, nonatomic) IBOutlet UILabel *ceilingLabel;
@property (strong, nonatomic) IBOutlet UILabel *deadloadLabel;
@property (strong, nonatomic) IBOutlet UILabel *windLabel;
@property (strong, nonatomic) IBOutlet UILabel *endBearingLabel;
@property (strong, nonatomic) IBOutlet UILabel *internalBearingLabel;

@end
