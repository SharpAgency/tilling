//
//  RaftersViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "RaftersViewController.h"
#import "ModelWebViewController.h"
#import "RafterCalcViewController.h"

#define SINGLE_SPAN_IMAGE_NAME @"rafter_single"
#define CONTINUOUS_SPAN_IMAGE_NAME @"rafter_continuous"

@interface RaftersViewController (){
    NSString *modelImageName;
    NSArray *results;
}
@end

@implementation RaftersViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Below I have created separate methods to handle object creation
    // in order to keep ViewDidLoad looking clean.
    // This class also contains methods linked to IB for textField change events
    
    [self addDataToPickerViewArrays];
    [self createPickerViews];
    
    // After above init, set input for textfield to pickerviews
    [self.spanConditionTextField setInputView:_spanConditionPickerView];
    [self.deadLoadTextField setInputView:_deadLoadPickerView];
    [self.rafterSpacingTextField setInputView:_rafterSpacingPickerView];
    [self.windSpeedTextField setInputView:_windSpeedPickerView];
    [self.productTextField setInputView:_productPickerView];
    
    // Set AccessoryViews
    [self.spanTextField setInputAccessoryView:_accessoryView];
    [self.spanConditionTextField setInputAccessoryView:_accessoryView];
    [self.deadLoadTextField setInputAccessoryView:_accessoryView];
    [self.rafterSpacingTextField setInputAccessoryView:_accessoryView];
    [self.windSpeedTextField setInputAccessoryView:_accessoryView];
    [self.productTextField setInputAccessoryView:_accessoryView];
    
    // Set string defaults
    self.spanInt = 0;
    self.spanConditionString = @"Single Span";
    self.deadLoadString = @"10";
    self.ceilingAttachedString = @"2";
    self.rafterSpacingString = @"450";
    self.windSpeedString = @"N1";
    self.productString = @"SmartJoist";
    
    modelImageName = SINGLE_SPAN_IMAGE_NAME;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 9;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // Check and activate appropriate textfields depending on cell selected
    if (indexPath.row == 0) [self.spanTextField becomeFirstResponder];
    if (indexPath.row == 1) [self.spanConditionTextField becomeFirstResponder];
    if (indexPath.row == 3) [self.deadLoadTextField becomeFirstResponder];
    if (indexPath.row == 4) [self.rafterSpacingTextField becomeFirstResponder];
    if (indexPath.row == 5) [self.windSpeedTextField becomeFirstResponder];
    if (indexPath.row == 6) [self.productTextField becomeFirstResponder];
    if (indexPath.row == 7) [self calculate];
    if (indexPath.row == 8) {
        if ([self.spanConditionString isEqualToString:@"Single Span"]) {
            modelImageName = SINGLE_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
        else if ([self.spanConditionString isEqualToString:@"Continuous Span"]) {
            modelImageName = CONTINUOUS_SPAN_IMAGE_NAME;
            [self performSegueWithIdentifier:@"modelSegue" sender:self];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove all first responders
    [self.view endEditing:YES];
}

#pragma mark - UIPickerView Data Source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray count];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray count];
    if (pickerView == _rafterSpacingPickerView) return [_rafterSpacingArray count];
    if (pickerView == _windSpeedPickerView) return [_windSpeedArray count];
    if (pickerView == _productPickerView) return [_productArray count];
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == _spanConditionPickerView) return [_spanConditionArray objectAtIndex:row];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray objectAtIndex:row];
    if (pickerView == _rafterSpacingPickerView) return [_rafterSpacingArray objectAtIndex:row];
    if (pickerView == _windSpeedPickerView) return [_windSpeedArray objectAtIndex:row];
    if (pickerView == _productPickerView) return [_productArray objectAtIndex:row];
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // set textviews based on selection made in pickerView
    if (pickerView == _spanConditionPickerView) {
        self.spanConditionTextField.text = [_spanConditionArray objectAtIndex:row];
        self.spanConditionString = [_spanConditionArray objectAtIndex:row];
    }
    if (pickerView == _deadLoadPickerView) {
        self.deadLoadTextField.text = [NSString stringWithFormat:@"%@kg/m2", [_deadLoadArray objectAtIndex:row]];
        self.deadLoadString = [_deadLoadArray objectAtIndex:row];
    }
    if (pickerView == _rafterSpacingPickerView) {
        self.rafterSpacingTextField.text = [NSString stringWithFormat:@"%@mm", [_rafterSpacingArray objectAtIndex:row]];
        self.rafterSpacingString = [_rafterSpacingArray objectAtIndex:row];
    }
    if (pickerView == _windSpeedPickerView) {
        self.windSpeedTextField.text =  [_windSpeedArray objectAtIndex:row];
        self.windSpeedString = [_windSpeedArray objectAtIndex:row];
    }
    if (pickerView == _productPickerView) {
        self.productTextField.text = [_productArray objectAtIndex:row];
        self.productString = [_productArray objectAtIndex:row];
    }
}

#pragma mark - IB methods for textField changes
- (IBAction)spanEditingDidChange:(id)sender {
    self.spanInt = [_spanTextField.text intValue];
}

- (IBAction)ceilingSwitch:(UISwitch *)sender {
    if ([sender isOn]) {
        self.ceilingAttachedString = @"1";
        _deadLoadArray = @[@"30",
                           @"40",
                           @"75",
                           @"90"];
        _deadLoadTextField.placeholder = @"30kg/m2";
        [self.deadLoadPickerView reloadAllComponents];
        _deadLoadTextField.text = @"30kg/m2";
        _deadLoadString = @"30";
    }
    else {
        self.ceilingAttachedString = @"2";
        _deadLoadArray = @[@"10",
                           @"20",
                           @"40",
                           @"60"];
        _deadLoadTextField.placeholder = @"10kg/m2";
        [self.deadLoadPickerView reloadAllComponents];
        _deadLoadTextField.text = @"10kg/m2";
        _deadLoadString = @"10";
    }
}

#pragma mark - init methods
- (void)createPickerViews {
    // Separate pickerviews for each occasion
    // makes it a little easier to make smaller custom changes and helps with debugging
    
    // Set up accessoryView. Used to dismiss pickerViews
    self.accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [self.accessoryView setItems:@[_doneButton]];
    
    // SpanCondition PickerView
    self.spanConditionPickerView = [[UIPickerView alloc] init];
    self.spanConditionPickerView.delegate = self;
    self.spanConditionPickerView.dataSource = self;
    
    // DeadLoad PickerView
    self.deadLoadPickerView = [[UIPickerView alloc] init];
    self.deadLoadPickerView.delegate = self;
    self.deadLoadPickerView.dataSource = self;
    
    // RafterSpacing PickerView
    self.rafterSpacingPickerView = [[UIPickerView alloc] init];
    self.rafterSpacingPickerView.delegate = self;
    self.rafterSpacingPickerView.dataSource = self;
    
    // WindSpeed PickerView
    self.windSpeedPickerView = [[UIPickerView alloc] init];
    self.windSpeedPickerView.delegate = self;
    self.windSpeedPickerView.dataSource = self;
    
    // Product PickerView
    self.productPickerView = [[UIPickerView alloc] init];
    self.productPickerView.delegate = self;
    self.productPickerView.dataSource = self;
}

- (void)doneButtonPressed {
    [self.view endEditing:YES];
}

- (void)addDataToPickerViewArrays {
    self.spanConditionArray = @[@"Single Span",
                                @"Continuous Span"];
    
    self.deadLoadArray = @[@"30",
                           @"40",
                           @"75",
                           @"90"];
    
    self.rafterSpacingArray = @[@"450",
                                @"600",
                                @"900",
                                @"1200"];
    
    self.windSpeedArray = @[@"N1",
                            @"N2",
                            @"N3",
                            @"N4",
                            @"C1",
                            @"C2",
                            @"C3"];
    
    self.productArray = @[@"SmartJoist",
                          @"SmartLVL15"];
}

#pragma mark - Perform Calculation
- (void)calculate {
    // This Method will filter through a .plist database creating an array at the end depending on user inputs.
    
    // Load data to filter
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"rafterData" ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];

    // set span condition to integer to match with datasource value
    NSString *spanConditionNumber = @"0";
    if ([_spanConditionString isEqualToString:@"Single Span"]) spanConditionNumber = @"1";
    else if ([_spanConditionString isEqualToString:@"Continuous Span"]) spanConditionNumber = @"2";
    
    
    // Set windspeed to integer to match with the datasource values. Aparently these are in 2 groups.
    NSString *windSpeedNumber = @"0";
    if ([_windSpeedString isEqualToString:@"N1"] ||
        [_windSpeedString isEqualToString:@"N2"] ||
        [_windSpeedString isEqualToString:@"N3"] ||
        [_windSpeedString isEqualToString:@"N4"]) windSpeedNumber = @"1";
    
    else if ([_windSpeedString isEqualToString:@"C1"] ||
             [_windSpeedString isEqualToString:@"C2"] ||
             [_windSpeedString isEqualToString:@"C3"]) windSpeedNumber = @"2";
    
    // set product id to integer to match with datasource value
    NSString *productIDNumber = @"0";
    if ([_productString isEqualToString:@"SmartJoist"]) productIDNumber = @"2";
    else if ([_productString isEqualToString:@"SmartLVL15"]) productIDNumber = @"1";
    
    
    // 1 SpanCondition Filter
    NSPredicate *spanConditionPredicate = [NSPredicate predicateWithFormat:@"SELF.spanConditionID CONTAINS %@", spanConditionNumber];
    NSArray *filteredBySpanCondition = [NSArray arrayWithArray:[contentArray filteredArrayUsingPredicate:spanConditionPredicate]];
    
    // 2 Ceiling Attatched Filter
    NSPredicate *ceilingSwitchPredicate = [NSPredicate predicateWithFormat:@"SELF.ceilingAttachedID CONTAINS %@", _ceilingAttachedString];
    NSArray *filteredBySwitch = [NSArray arrayWithArray:[filteredBySpanCondition filteredArrayUsingPredicate:ceilingSwitchPredicate]];
    
    // 3 Dead Load Filter
    NSPredicate *deadloadPredicate = [NSPredicate predicateWithFormat:@"SELF.roofDL CONTAINS %@", _deadLoadString];
    NSArray *filteredByDeadload = [NSArray arrayWithArray:[filteredBySwitch filteredArrayUsingPredicate:deadloadPredicate]];
    
    // 4 Rafter Spacing Filter
    NSPredicate *rafterSpacingPredicate = [NSPredicate predicateWithFormat:@"SELF.rafter_spacing CONTAINS %@", _rafterSpacingString];
    NSArray *filteredByRafter = [NSArray arrayWithArray:[filteredByDeadload filteredArrayUsingPredicate:rafterSpacingPredicate]];
    
    // 5 WindSpeed filter
    NSPredicate *windSpeedPredicate = [NSPredicate predicateWithFormat:@"SELF.windSpeedGroup CONTAINS %@", windSpeedNumber];
    NSArray *filteredByWindspeed = [NSArray arrayWithArray:[filteredByRafter filteredArrayUsingPredicate:windSpeedPredicate]];
    
    // 6 product filter
    NSPredicate *productPredicate = [NSPredicate predicateWithFormat:@"SELF.productID CONTAINS %@", productIDNumber];
    NSArray *filteredByProduct = [NSArray arrayWithArray:[filteredByWindspeed filteredArrayUsingPredicate:productPredicate]];
    
    // 7 span Filter
    NSPredicate *spanPredicate = [NSPredicate predicateWithFormat:@"SELF.span.intValue >= %d", [_spanTextField.text intValue]];
    NSArray *filteredBySpan = [NSArray arrayWithArray:[filteredByProduct filteredArrayUsingPredicate:spanPredicate]];
    
    results = filteredBySpan;

    NSLog(@"Raw Count: %lu", (unsigned long)[contentArray count]);
    NSLog(@"Span Filter Count: %lu", (unsigned long)[filteredBySpanCondition count]);
    NSLog(@"Switch Filter Count: %lu", (unsigned long)[filteredBySwitch count]);
    NSLog(@"deadload Count: %lu", (unsigned long)[filteredByDeadload count]);
    NSLog(@"Rafter Count: %lu", (unsigned long)[filteredByRafter count]);
    NSLog(@"windspeed Count: %lu", (unsigned long)[filteredByWindspeed count]);
    NSLog(@"product Count: %lu", (unsigned long)[filteredByProduct count]);
    NSLog(@"span Count: %lu", (unsigned long)[filteredBySpan count]);

    [self performSegueWithIdentifier:@"resultsSegue" sender:self];
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"modelSegue"]) {
        ModelWebViewController *modelView = [segue destinationViewController];
        [modelView setImageName:modelImageName];
    }
    // Get data from .plist, put into array and add to next viewController
    if ([[segue identifier] isEqualToString:@"resultsSegue"]) {
        
        RafterCalcViewController *vc = [segue destinationViewController];
        [vc setContent:results];
        [vc setWindSpeed:_windSpeedString];
    }
}
@end
