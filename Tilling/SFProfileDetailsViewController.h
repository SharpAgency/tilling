//
//  SFProfileDetailsViewController.h
//  Tilling
//
//  Created by Beau Young on 8/01/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollImageView.h"

@interface SFProfileDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet ScrollImageView *scrollingImageView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *arrayOfData;
@property (strong, nonatomic) NSArray *arrayOfTitles;
@property (strong, nonatomic) NSArray *colorArray;

@property (strong, nonatomic) NSMutableDictionary *data;
@property (strong, nonatomic) NSString *imagePath;

@end
