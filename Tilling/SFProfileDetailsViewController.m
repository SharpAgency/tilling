//
//  SFProfileDetailsViewController.m
//  Tilling
//
//  Created by Beau Young on 8/01/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "SFProfileDetailsViewController.h"

@interface SFProfileDetailsViewController ()

@end

@implementation SFProfileDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blueGrid"]];
        
    self.arrayOfTitles = @[@"Product",
                           @"Species",
                           @"Treatment",
                           @"Application"];
    
    self.arrayOfData = @[[_data objectForKey:@"product"],
                         [_data objectForKey:@"species"],
                         [_data objectForKey:@"treatment"],
                         [_data objectForKey:@"application"]];

    self.colorArray = @[[UIColor colorWithHue:0.516 saturation:0.250 brightness:0.921 alpha:1.000],
                        [UIColor colorWithHue:0.516 saturation:0.350 brightness:0.921 alpha:1.000],
                        [UIColor colorWithHue:0.516 saturation:0.450 brightness:0.921 alpha:1.000],
                        [UIColor colorWithHue:0.516 saturation:0.550 brightness:0.921 alpha:1.000]];
    
    self.scrollingImageView = [[ScrollImageView alloc] initWithFrame:self.scrollingImageView.frame andImagePath:[_data objectForKey:@"imageArray"]];
    
    ScrollImageView *imageAnimation = [[ScrollImageView alloc] initWithFrame:self.scrollingImageView.frame andImagePath:[_data objectForKey:@"imageArray"]];
    
    [self.view addSubview:imageAnimation];
}

#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set Cell identifier
    NSString *CellIdentifier = [_arrayOfTitles objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // configure the cell
    cell.textLabel.text = [_arrayOfTitles objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [_arrayOfData objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [_colorArray objectAtIndex:indexPath.row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
