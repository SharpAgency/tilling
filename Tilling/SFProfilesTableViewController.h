//
//  SFProfilesTableViewController.h
//  Tilling
//
//  Created by Beau Young on 8/01/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFProfilesTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *images;
@property (strong, nonatomic) NSArray *titles;
@property (strong, nonatomic) NSArray *products;

@end
