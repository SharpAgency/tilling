//
//  SSPoductProfilesTableViewController.m
//  Tilling
//
//  Created by Beau Young on 6/01/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "SSPoductProfilesTableViewController.h"
#import "ArchitecturalTableViewCell.h"
#import "SSProductDetailsViewController.h"

@interface SSPoductProfilesTableViewController ()

@end

@implementation SSPoductProfilesTableViewController {
    NSIndexPath *savedIndexpath;

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.products = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"smartStructProducts" ofType:@"plist"]];
//    self.tableView.backgroundColor = [UIColor colorWithRed:119/255.0 green:187/255.0 blue:19/255.0 alpha:1.000];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.products count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set Cell identifier
    static NSString *CellIdentifier = @"Cell";
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // If no cell, create one with custom nib
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // Configure the cell...
    cell.titleLabel.text = [[self.products objectAtIndex:indexPath.row] valueForKey:@"product"];
    cell.detailLabel.text = nil;
    
    cell.noShadow = YES;
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row % 2) {
        cell.backgroundColor = [UIColor colorWithRed:0.527 green:0.751 blue:0.166 alpha:0.600];
    }else {
        cell.backgroundColor = [UIColor colorWithRed:0.587 green:0.811 blue:0.226 alpha:0.600];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    savedIndexpath = [[NSIndexPath alloc] init];
    savedIndexpath = indexPath;
    
    [self performSegueWithIdentifier:@"productDetailsSegue" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    SSProductDetailsViewController *vc = [segue destinationViewController];
    [vc setData:[self.products objectAtIndex:savedIndexpath.row]];
}


@end
