//
//  SSProductDetailsViewController.m
//  Tilling
//
//  Created by Beau Young on 6/01/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "SSProductDetailsViewController.h"

@interface SSProductDetailsViewController ()

@end

@implementation SSProductDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"greenGrid"]];
    
    self.arrayOfTitles = @[@"Product",
                           @"Species",
                           @"Treatment",
                           @"Application"];
    
    self.arrayOfData = @[[_data objectForKey:@"product"],
                         [_data objectForKey:@"species"],
                         [_data objectForKey:@"treatment"],
                         [_data objectForKey:@"application"]];
    
    self.colorArray = @[[UIColor colorWithHue:0.337 saturation:0.180 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.337 saturation:0.280 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.337 saturation:0.380 brightness:0.949 alpha:1.000],
                        [UIColor colorWithHue:0.337 saturation:0.480 brightness:0.949 alpha:1.000]];
    
    self.scrollingImageView = [[ScrollImageView alloc] initWithFrame:self.scrollingImageView.frame andImagePath:[_data objectForKey:@"imageArray"]];
    
    ScrollImageView *imageAnimation = [[ScrollImageView alloc] initWithFrame:self.scrollingImageView.frame andImagePath:[_data objectForKey:@"imageArray"]];
    
    [self.view addSubview:imageAnimation];
    NSLog(@"%@", _arrayOfData);
}

#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set Cell identifier
    NSString *CellIdentifier = [_arrayOfTitles objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // configure the cell
    cell.textLabel.text = [_arrayOfTitles objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [_arrayOfData objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [_colorArray objectAtIndex:indexPath.row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
