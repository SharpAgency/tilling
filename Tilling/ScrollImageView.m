//
//  ScrollImageView.m
//  Tilling
//
//  Created by Beau Young on 16/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#define DURATION 1

#import "ScrollImageView.h"

@implementation ScrollImageView {
    
    // for storing the image path
    NSString *imagePathStored;
    
}

-(id)initWithFrame:(CGRect)frame andImagePath:(NSString *)imagePath {
    self = [super initWithFrame:frame];
    if (self) {
        
        imagePathStored = imagePath;
        
        // other setup for the view
        self.contentMode = UIViewContentModeScaleAspectFit;
        self.userInteractionEnabled = YES;
        self.currentFrame = 1;
        self.totalFrames = 40;
        
        // Load Images with names
        NSMutableArray *imageNames = [[NSMutableArray alloc] init];
        for (int i = 1; i <= 40; i++) [imageNames addObject:[NSString stringWithFormat:@"%@%d", imagePath, i]];
        
        // Create array of images
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (int i = 0; i < imageNames.count; i++) [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
        
        // set first image
        self.image = [images objectAtIndex:0];
        
        // set the array of images to animate
        self.animationImages = images;

        // how fast the images will animate
        self.animationDuration = DURATION;
        [self startAnimating];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    startPt = [touch locationInView:nil];
    startFrame = self.currentFrame;
    
    [self stopAnimating];
    [self changeParentScrolling:false];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint currentPt = [touch locationInView:nil];
    
    float distance = currentPt.x - startPt.x;
    
    int totalFrameChange = floor(distance/15);
    
    int changeFrame = startFrame+totalFrameChange;
    if (changeFrame <= 0) {
        changeFrame = self.totalFrames+changeFrame;
    } else if (changeFrame > self.totalFrames) {
        changeFrame = changeFrame-self.totalFrames;
    }
    
    if (changeFrame != self.currentFrame) {
        self.currentFrame = changeFrame;
        NSString *fileName = [imagePathStored stringByAppendingFormat:@"%d", self.currentFrame];
        [self setImage:[UIImage imageNamed:fileName]];
    }
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self changeParentScrolling:true];
//    [self startAnimating];
}

#pragma mark - Start/Stop Parent Scrolling
-(void)changeParentScrolling:(bool)scroll{
    
    //stop parent UIScrollView from moving
    UIView* parentView = [self superview];
    if(parentView.class == UIScrollView.class){
        UIScrollView* parent = (UIScrollView*)parentView;
        [parent setScrollEnabled:scroll];
    }
}

@end
