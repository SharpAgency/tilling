//
//  SmartJoistCalcViewController.m
//  Tilling
//
//  Created by Maris on 7/11/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartJoistCalcViewController.h"
#import "CustomCalculatorCell.h"
#import "SmartJoistDetailsViewController.h"

@interface SmartJoistCalcViewController ()

@end

@implementation SmartJoistCalcViewController {
    NSDictionary *detailsContent;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set Cell identifier
    
    // if content exists, display it................
    if ([_content count]) {
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.text = _productRange;
        cell.textLabel.textColor = [UIColor whiteColor];
        
        // check if row is odd or even and set color accordingly
        if (indexPath.row % 2) {
            cell.backgroundColor = [UIColor colorWithRed:0.408 green:0.840 blue:0.938 alpha:0.600];
        }else {
            cell.backgroundColor = [UIColor colorWithRed:0.086 green:0.720 blue:0.898 alpha:0.600];
        }
        
        return cell;
    }
    // if content doesnt exist, inform the user.................
    else {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.text = @"No results within bounds of calculation";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
        
        tableView.separatorColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor colorWithRed:0.408 green:0.840 blue:0.938 alpha:0.600];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    detailsContent = [_content objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"detailsSegue" sender:nil];
}


#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"detailsSegue"]) {
        SmartJoistDetailsViewController *detailsView = [segue destinationViewController];
        [detailsView setJoistSpan:_productRange];
        [detailsView setContent:detailsContent];
    }
}

@end
