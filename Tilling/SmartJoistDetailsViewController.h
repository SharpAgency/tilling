//
//  SmartJoistDetailsViewController.h
//  Tilling
//
//  Created by Beau Young on 24/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmartJoistDetailsViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary *content;

@property (strong, nonatomic) IBOutlet UILabel *joistSizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *joistSpacingLabel;
@property (strong, nonatomic) IBOutlet UILabel *joistSpanLabel;
@property (strong, nonatomic) IBOutlet UILabel *holeShapeLabel;
@property (strong, nonatomic) IBOutlet UILabel *holeDiameterLabel;
@property (strong, nonatomic) IBOutlet UILabel *deadloadLabel;
@property (strong, nonatomic) IBOutlet UILabel *minDistLabel;

@property (strong, nonatomic) NSString *joistSpan;


@end
