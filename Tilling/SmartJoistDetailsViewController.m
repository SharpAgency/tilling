//
//  SmartJoistDetailsViewController.m
//  Tilling
//
//  Created by Beau Young on 24/12/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartJoistDetailsViewController.h"
#import <MessageUI/MessageUI.h>

@interface SmartJoistDetailsViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation SmartJoistDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.joistSizeLabel.text = [_content objectForKey:@"ID"];
    
    self.joistSpacingLabel.text = [_content objectForKey:@"spacing"];
    
    self.joistSpanLabel.text = _joistSpan;
    
    if ([[_content objectForKey:@"shape"] isEqualToString:@"1"]) self.holeShapeLabel.text = @"Circular";
    else if ([[_content objectForKey:@"shape"] isEqualToString:@"2"]) self.holeShapeLabel.text = @"Rectangular";
    
    self.holeDiameterLabel.text = [_content objectForKey:@"holeDiameter"];
    
    if ([[_content objectForKey:@"deadload"] isEqualToString:@"1"]) self.deadloadLabel.text = @"40kg/m2, 1.5kPa";
    else if ([[_content objectForKey:@"deadload"] isEqualToString:@"2"]) self.deadloadLabel.text = @"62kg/m2, 2.0kPa";
    else if ([[_content objectForKey:@"deadload"] isEqualToString:@"3"]) self.deadloadLabel.text = @"100kg/m2, 2.0kPa";
    
    self.minDistLabel.text = [_content objectForKey:@"distance"];
}

- (IBAction)emailBtn:(UIBarButtonItem *)sender {
    // email subject
    NSString *emailSubject = @"Tilling Building Calculator - Your Results";
    NSString *messageBody = [NSString stringWithFormat:@"Joist Size: %@\n Joist Spacing: %@\n Joist Span: %@\n Hole Shape: %@\n Hole Diameter (mm): %@\n Floor Dead/Live Load: %@\n Min. distance from any support to the center of the hole (mm): %@\n",
                             _joistSizeLabel.text,
                             _joistSpacingLabel.text,
                             _joistSpanLabel.text,
                             _holeShapeLabel.text,
                             _holeDiameterLabel.text,
                             _deadloadLabel.text,
                             _minDistLabel.text];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailSubject];
    [mc setMessageBody:messageBody isHTML:NO];
    // present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

@end
