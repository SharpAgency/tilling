//
//  SmartJoistViewController.h
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldWithNoCaret.h"

@interface SmartJoistViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource>
// Each property is declared in order of appearance.
@property (weak, nonatomic) IBOutlet UITextField *spanTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *joistSpacingTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *holeShapeTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *holeSizeTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *jointSizeTextField;
@property (weak, nonatomic) IBOutlet TextFieldWithNoCaret *deadLoadTextField;

// NSStrings for storing TextField Values
@property int spanInt;
@property (strong, nonatomic) NSString *joistSpacingString;
@property (strong, nonatomic) NSString *holeShapeString;
@property (strong, nonatomic) NSString *holeSizeString;
@property (strong, nonatomic) NSString *jointSizeString;
@property (strong, nonatomic) NSString *deadLoadString;

// UIPickerViews for each textField

@property (strong, nonatomic) UIPickerView *joistSpacingPickerView;
@property (strong, nonatomic) UIPickerView *holeShapePickerView;
@property (strong, nonatomic) UIPickerView *holeSizePickerView;
@property (strong, nonatomic) UIPickerView *jointSizePickerView;
@property (strong, nonatomic) UIPickerView *deadLoadPickerView;

// UIPickerView ToolBar - Done Button.
@property (strong, nonatomic) UIToolbar *accessoryView;
@property (strong, nonatomic) UIBarButtonItem *doneButton;

// UIPickerView data arrays
@property (strong, nonatomic) NSArray *joistSpacingArray;
@property (strong, nonatomic) NSArray *holeShapeArray;
@property (strong, nonatomic) NSArray *holeSizeCircularArray;
@property (strong, nonatomic) NSArray *holeSizeRectangularArray;
@property (strong, nonatomic) NSArray *jointSizeArray;
@property (strong, nonatomic) NSArray *deadLoadArray;

// Label for changing
@property (strong, nonatomic) IBOutlet UILabel *holeSizeLabel;

@end
