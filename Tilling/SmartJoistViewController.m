//
//  SmartJoistViewController.m
//  Tilling
//
//  Created by Maris on 15/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartJoistViewController.h"
#import "ModelWebViewController.h"
#import "SmartJoistCalcViewController.h"

#define SINGLE_SPAN_IMAGE_NAME @"webhole_single"

@interface SmartJoistViewController () {
    NSString *modelImageName;
    NSArray *results;
    NSArray *holeSizeVar;
    NSString *_rangeString;
}

@end

@implementation SmartJoistViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Below I have created separate methods to handle object creation
    // in order to keep ViewDidLoad looking clean.
    // This class also contains methods linked to IB for textField change events
    
    [self createPickerViews];
    [self addDataToPickerViewArrays];
    
    // After above init, set input for textfields to pickerviews.
    [self.joistSpacingTextField setInputView:_joistSpacingPickerView];
    [self.holeShapeTextField setInputView:_holeShapePickerView];
    [self.holeSizeTextField setInputView:_holeSizePickerView];
    [self.jointSizeTextField setInputView:_jointSizePickerView];
    [self.deadLoadTextField setInputView:_deadLoadPickerView];
    
    // Set accessoryViews
    [self.spanTextField setInputAccessoryView:_accessoryView];
    [self.joistSpacingTextField setInputAccessoryView:_accessoryView];
    [self.holeShapeTextField setInputAccessoryView:_accessoryView];
    [self.holeSizeTextField setInputAccessoryView:_accessoryView];
    [self.jointSizeTextField setInputAccessoryView:_accessoryView];
    [self.deadLoadTextField setInputAccessoryView:_accessoryView];
    
    // Set string defaults
    self.spanInt = 0;
    self.joistSpacingString = @"300 to 600";
    self.holeShapeString = @"Circular";
    self.holeSizeString = @"75";
    self.jointSizeString = @"SJ20044";
    self.deadLoadString = @"40kg/m2, 1.5kPa";
    
    // set up holeSizeVar Array
    holeSizeVar = _holeSizeCircularArray;
    
    // Set model image name
    modelImageName = SINGLE_SPAN_IMAGE_NAME;
}

#pragma mark - IB methods for textField changes
- (IBAction)spanTextfieldDidEnd:(id)sender {
    self.spanInt = [_spanTextField.text intValue];
}

#pragma mark - init methods
- (void)createPickerViews {
    // Separate pickerviews for each occasion
    // makes it a little easier to make smaller custom changes and helps with debugging
    
    // Set up accessoryView. Used to dismiss pickerViews
    self.accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed)];
    [self.accessoryView setItems:@[_doneButton]];
    
    
    // Joist spacing PickerView
    self.joistSpacingPickerView = [[UIPickerView alloc] init];
    self.joistSpacingPickerView.delegate = self;
    self.joistSpacingPickerView.dataSource = self;
    
    // HoleShape PickerView
    self.holeShapePickerView = [[UIPickerView alloc] init];
    self.holeShapePickerView.delegate = self;
    self.holeShapePickerView.dataSource = self;
    
    // HoleSize PickerView
    self.holeSizePickerView = [[UIPickerView alloc] init];
    self.holeSizePickerView.delegate = self;
    self.holeSizePickerView.dataSource = self;
    
    // JointSize pickerView
    self.jointSizePickerView = [[UIPickerView alloc] init];
    self.jointSizePickerView.delegate = self;
    self.jointSizePickerView.dataSource = self;
    
    // deadLoad PickerView
    self.deadLoadPickerView = [[UIPickerView alloc] init];
    self.deadLoadPickerView.delegate = self;
    self.deadLoadPickerView.dataSource = self;
}
- (void)doneButtonPressed {
    [self.view endEditing:YES];
}

- (void)addDataToPickerViewArrays {
    self.joistSpacingArray = @[@"300 to 600"];
    
    
    self.holeShapeArray = @[@"Circular", @"Rectangular"];
    
    
    self.holeSizeCircularArray = @[@"75", @"100", @"125", @"150",
                                   @"175", @"200", @"225", @"250"];
    
    self.holeSizeRectangularArray = @[@"125x150", @"150x300", @"175x350", @"200x400"];
    
    self.jointSizeArray = @[@"SJ20044", @"SJ24040", @"SJ24051", @"SJ24070",
                            @"SJ24090", @"SJ30040", @"SJ30051", @"SJ30070",
                            @"SJ30090", @"SJ36058", @"SJ36090", @"SJ40090"];
    
    self.deadLoadArray = @[@"40kg/m2, 1.5kPa", @"62kg/m2, 2.0kPa", @"100kg/m2, 2.0kPa"];
}

#pragma mark - TableView delegate and Datasource
// CellForRow.... not needed when using static cells
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // return the number of rows in each section
    return 8;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // check and activate appropriate textfields depending on cell selected
    if (indexPath.row == 0) [self.spanTextField becomeFirstResponder];
    if (indexPath.row == 1) [self.joistSpacingTextField becomeFirstResponder];
    if (indexPath.row == 2) [self.holeShapeTextField becomeFirstResponder];
    if (indexPath.row == 3) [self.holeSizeTextField becomeFirstResponder];
    if (indexPath.row == 4) [self.jointSizeTextField becomeFirstResponder];
    if (indexPath.row == 5) [self.deadLoadTextField becomeFirstResponder];
    if (indexPath.row == 6) [self calculate];
    if (indexPath.row == 7) [self performSegueWithIdentifier:@"modelSegue" sender:self];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    // remove all first responders
    [self.view endEditing:YES];
}

#pragma mark - UIPickerView dataSource and Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == _joistSpacingPickerView) return [_joistSpacingArray count];
    if (pickerView == _holeShapePickerView) return [_holeShapeArray count];
    if (pickerView == _holeSizePickerView) return [holeSizeVar count];
    if (pickerView == _jointSizePickerView) return [_jointSizeArray count];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray count];
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == _joistSpacingPickerView) return [_joistSpacingArray objectAtIndex:row];
    if (pickerView == _holeShapePickerView) return [_holeShapeArray objectAtIndex:row];
    if (pickerView == _holeSizePickerView) return [holeSizeVar objectAtIndex:row];
    if (pickerView == _jointSizePickerView) return [_jointSizeArray objectAtIndex:row];
    if (pickerView == _deadLoadPickerView) return [_deadLoadArray objectAtIndex:row];
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // set textviews based on selection made in pickerview
    if (pickerView == _joistSpacingPickerView) {
        self.joistSpacingTextField.text = [_joistSpacingArray objectAtIndex:row];
        self.joistSpacingString = [_joistSpacingArray objectAtIndex:row];
    }
    if (pickerView == _holeShapePickerView) {
        self.holeShapeTextField.text = [_holeShapeArray objectAtIndex:row];
        self.holeShapeString = [_holeShapeArray objectAtIndex:row];
        if (row == 0) {
            self.holeSizeTextField.placeholder = @"75mm";
            self.holeSizeTextField.text = nil;
            self.holeSizeLabel.text = @"Hole Diameter";
            holeSizeVar = _holeSizeCircularArray;
            self.holeSizeString = [holeSizeVar objectAtIndex:row];
            [self.holeSizePickerView reloadAllComponents];
        }
        if (row == 1) {
            self.holeSizeTextField.placeholder = @"125x150mm";
            self.holeSizeTextField.text = nil;
            self.holeSizeLabel.text = @"Hole Depth/Width";
            holeSizeVar = _holeSizeRectangularArray;
            self.holeSizeString = [holeSizeVar objectAtIndex:0];
            [self.holeSizePickerView reloadAllComponents];
        }
    }
    if (pickerView == _holeSizePickerView) {
        self.holeSizeTextField.text = [NSString stringWithFormat:@"%@mm", [holeSizeVar objectAtIndex:row]];
        self.holeSizeString = [holeSizeVar objectAtIndex:row];
    }
    if (pickerView == _jointSizePickerView) {
        self.jointSizeTextField.text = [_jointSizeArray objectAtIndex:row];
        self.jointSizeString = [_jointSizeArray objectAtIndex:row];
    }
    if (pickerView == _deadLoadPickerView) {
        self.deadLoadTextField.text = [_deadLoadArray objectAtIndex:row];
        self.deadLoadString = [_deadLoadArray objectAtIndex:row];
    }
}

- (void)calculate {
    // This Method will filter through a .plist database creating an array at the end depending on user inputs.
    
    // Load data to filter
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"WebHole Data" ofType:@"plist"];
    NSArray *contentArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    // set deadload to number to match the plist
    NSString *deadLoadNumber = @"0";
    if ([_deadLoadString isEqualToString:@"40kg/m2, 1.5kPa"]) deadLoadNumber = @"1";
    else if ([_deadLoadString isEqualToString:@"62kg/m2, 2.0kPa"]) deadLoadNumber = @"2";
    else if ([_deadLoadString isEqualToString:@"100kg/m2, 2.0kPa"]) deadLoadNumber = @"3";
    
    // set holeShape to number to match the plist
    NSString *holeShapeNumber = @"0";
    if ([_holeShapeString isEqualToString:@"Circular"]) holeShapeNumber = @"1";
    else if ([_holeShapeString isEqualToString:@"Rectangular"]) holeShapeNumber = @"2";
    
    // 1 HoleShape filter
    NSPredicate *holeShapePredicate = [NSPredicate predicateWithFormat:@"SELF.shape CONTAINS %@", holeShapeNumber];
    NSArray *filteredByHoleShape = [NSArray arrayWithArray:[contentArray filteredArrayUsingPredicate:holeShapePredicate]];
    
    // 2 DeadLoad Filter
    NSPredicate *deadLoadPredicate = [NSPredicate predicateWithFormat:@"SELF.deadload CONTAINS %@", deadLoadNumber];
    NSArray *filteredByDeadload = [NSArray arrayWithArray:[filteredByHoleShape filteredArrayUsingPredicate:deadLoadPredicate]];
    
    // 3 Span Filter
    _spanInt = [_spanTextField.text intValue];
    int topNumber = 10;
    int bottomNumber = 0;
    if (_spanInt >= 999 && _spanInt <= 1499) {
        topNumber = 1499;
        bottomNumber = 999;
        _rangeString = @"999 to 1499";
    }
    else if (_spanInt >= 1500 && _spanInt <= 1999) {
        topNumber = 1999;
        bottomNumber = 1500;
        _rangeString = @"1500 to 1999";
    }
    else if (_spanInt >= 2000 && _spanInt <= 2499) {
        topNumber = 2499;
        bottomNumber = 2000;
        _rangeString = @"2000 to 2499";
    }
    else if (_spanInt >= 2500 && _spanInt <= 2999) {
        topNumber = 2999;
        bottomNumber = 2500;
        _rangeString = @"2500 to 2999";
    }
    else if (_spanInt >= 3000 && _spanInt <= 3500) {
        topNumber = 3500;
        bottomNumber = 3000;
        _rangeString = @"3000 to 3500";
    }
    NSPredicate *spanPredicate = [NSPredicate predicateWithFormat:@"SELF.span.intValue >= %d && SELF.span.intValue <= %d", bottomNumber, topNumber];
    NSArray *filteredBySpan = [NSArray arrayWithArray:[filteredByDeadload filteredArrayUsingPredicate:spanPredicate]];
    
    // 4 JointSize Filter / product ID filter
    NSPredicate *sizePredicate = [NSPredicate predicateWithFormat:@"SELF.ID CONTAINS %@", _jointSizeString];
    NSArray *filteredBySize = [NSArray arrayWithArray:[filteredBySpan filteredArrayUsingPredicate:sizePredicate]];
    
    // 5 HoleSize Filter
    NSPredicate *holeSizePredicate = [NSPredicate predicateWithFormat:@"SELF.holeDiameter CONTAINS %@", _holeSizeString];
    NSArray *filteredByHoleSize = [NSArray arrayWithArray:[filteredBySize filteredArrayUsingPredicate:holeSizePredicate]];
    
    results = filteredByHoleSize;
    
    NSLog(@"Full List: %lu", (unsigned long) [contentArray count]);
    NSLog(@"Hole Shape Count: %lu", (unsigned long) [filteredByHoleShape count]);
    NSLog(@"Deadload count: %lu", (unsigned long) [filteredByDeadload count]);
    NSLog(@"Span count: %lu", (unsigned long) [filteredBySpan count]);
    NSLog(@"Size count: %lu", (unsigned long) [filteredBySize count]);
    NSLog(@"Hole Size count: %lu", (unsigned long) [filteredByHoleSize count]);

    [self performSegueWithIdentifier:@"resultsSegue" sender:self];
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"modelSegue"]) {
        ModelWebViewController *modelView = [segue destinationViewController];
        [modelView setImageName:SINGLE_SPAN_IMAGE_NAME];
    }
    
    if ([[segue identifier] isEqualToString:@"resultsSegue"]) {
        SmartJoistCalcViewController *vc = [segue destinationViewController];
        [vc setContent:results];
        [vc setProductRange:_rangeString];
    }
}

@end
