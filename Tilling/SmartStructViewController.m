//
//  SmartStructViewController.m
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "SmartStructViewController.h"
#import "ArchitecturalTableViewCell.h"

@implementation SmartStructViewController {
    NSIndexPath *selectedCellIndexPath;
    NSArray *cellBackgroundColorArray;
    NSArray *cellTitles;
    NSArray *cellDetailText;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.537 green:0.761 blue:0.176 alpha:1.000];
    
    cellBackgroundColorArray = @[[UIColor colorWithRed:0.527 green:0.751 blue:0.166 alpha:0.600],
                                 [UIColor colorWithRed:0.557 green:0.781 blue:0.196 alpha:0.600],
                                 [UIColor colorWithRed:0.587 green:0.811 blue:0.226 alpha:0.600],
                                 [UIColor colorWithRed:0.617 green:0.831 blue:0.246 alpha:0.600]
                                 ];
    
    cellTitles = @[@"SmartStruct",
                   @"Products",
                   @"Product Profiles",
                   @"Visit this sections website"];
    
    
    cellDetailText = @[@"Presently there is considerable interest in the construction of large-scale projects utilising timber. Timber is an ideal alternative to current construction methods with considerable environmental advantages, such as the storage of Carbon and Recyclability.\n\nA primary reason for building in timber is the speed of assembly. Timber has been found to be quicker to construct and easier to work with, not to mention considerable advantages in weight reduction with comparable strength alternative materials.\n\nConsiderable interest is focusing on Cross Laminated Timber(CLT). CLT is increasingly growing in popularity with extensive media attention on the viability and market acceptance of timber as an alternative commercial construction material. However, there exists a number of complementary products that accompany CLT. These include, Commercial I-Joist [500mm and 600mm], Cassette Modular Flooring Systems, Glue Laminated Beams and Post-tension Laminated Veneer Lumber (LVL) or Glulam Box Beams.",
                       
                       @"",
                       
                       @"",
                       @""];
    
}

#pragma mark - TableView delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableview cell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    // Configure the cells...
    cell.titleLabel.text = [cellTitles objectAtIndex:indexPath.section];
    cell.detailLabel.text = [cellDetailText objectAtIndex:indexPath.section];
    cell.backgroundColor = [cellBackgroundColorArray objectAtIndex:indexPath.section];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
    
    if (indexPath.section == 1) [self performSegueWithIdentifier:@"productsSegue" sender:nil];
    if (indexPath.section == 2) [self performSegueWithIdentifier:@"profilesSegue" sender:nil];
    if (indexPath.section == 3) [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tilling.com.au/smartstruct"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 56 points.
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.section == 0) return 550; // Expanded height
    }
    return 56;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 168;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
