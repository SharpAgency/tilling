//
//  StoreLocatorViewController.m
//  Tilling
//
//  Created by Maris on 7/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "StoreLocatorViewController.h"
#import "StoreTableViewCell.h"

@interface StoreLocatorViewController () <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UIActionSheetDelegate>

@end

@implementation StoreLocatorViewController {
    NSArray *companies;
    NSMutableArray *filteredContent;
    NSString *address;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.searchDisplayController.searchBar.placeholder = @"Postcode, Suburb, Company";
    companies = [[NSArray alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"storeData" ofType:@"plist"]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [filteredContent count];
    } else {
    return [companies count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *tableviewContent;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        tableviewContent = filteredContent;
    } else {
        tableviewContent = companies;
    }
    
    static NSString *CellIdentifier = @"Cell";
    
    StoreTableViewCell *cell = (StoreTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"storeTableViewCell" owner:self options:nil];
        cell = nib[0];
    }
    
    cell.companyNameLabel.text = [tableviewContent[indexPath.row] valueForKey:@"CompanyName"];
    cell.addressLabel.text = [tableviewContent[indexPath.row] valueForKey:@"FullAddress"];
    cell.suburbLabel.text = [tableviewContent[indexPath.row] valueForKey:@"Suburb"];
    cell.postCodeLabel.text = [tableviewContent[indexPath.row] valueForKey:@"Postcode"];
    [cell.phoneNumber setTitle:[tableviewContent[indexPath.row] valueForKey:@"Phone"] forState:UIControlStateNormal];
    [cell.phoneNumber addTarget:self action:@selector(numberTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Apple Maps", nil];
    [actionSheet showInView:self.view];
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        address = filteredContent[indexPath.row];
    } else {
        address = companies[indexPath.row];
    }
}

- (void)numberTapped:(UIButton *)sender {
    NSString *phoneNumber = [@"tel://" stringByAppendingString:sender.titleLabel.text];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

#pragma mark - ActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSString *addressString = [[[[NSString stringWithFormat:@"http://maps.apple.com/?q=%@", [address valueForKey:@"FullAddress"]] componentsSeparatedByCharactersInSet:doNotWant] componentsJoinedByString:@""] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:addressString]];
        NSLog(@"%@", addressString);
    }
}

#pragma mark - SearchDisplayDelegate
- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
    // Update the filtered array based on the search text and scope
    // remove all objects from the filtered array
    [filteredContent removeAllObjects];
    
    // Further order the array with the scope
    if ([scope isEqualToString:@"Engineered"]) {
        NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"Engineer" ascending:NO];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Suburb beginswith[c]%@ OR SELF.Postcode beginswith[c]%@ OR SELF.CompanyName beginswith[c]%@", searchText, searchText, searchText];
        filteredContent = [NSMutableArray arrayWithArray:[companies filteredArrayUsingPredicate:predicate]];
        [filteredContent sortUsingDescriptors:[NSArray arrayWithObject:sortDesc]];
    }
    else if ([scope isEqualToString:@"Architectural"]) {
        NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"Suburb" ascending:YES];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Suburb beginswith[c]%@ AND SELF.Architect != '' OR SELF.Postcode beginswith[c]%@ AND SELF.Architect != '' OR SELF.CompanyName beginswith[c]%@ AND SELF.Architect != ''", searchText, searchText, searchText];
        filteredContent = [NSMutableArray arrayWithArray:[companies filteredArrayUsingPredicate:predicate]];
        [filteredContent sortUsingDescriptors:[NSArray arrayWithObject:sortDesc]];
    }

    else {
        NSSortDescriptor *sortAlpha = [NSSortDescriptor sortDescriptorWithKey:@"Suburb" ascending:YES];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Suburb beginswith[c]%@ OR SELF.Postcode beginswith[c]%@ OR SELF.CompanyName beginswith[c] %@", searchText, searchText, searchText];
        filteredContent = [NSMutableArray arrayWithArray:[companies filteredArrayUsingPredicate:predicate]];
        [filteredContent sortUsingDescriptors:[NSArray arrayWithObject:sortAlpha]];
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Res YES to cause the search result table view to be reloaded.
    return YES;
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    //  Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView {
    [tableView setRowHeight:[[self tableView] rowHeight]];
    [tableView reloadData];
}
@end
