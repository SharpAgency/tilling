//
//  StoreTableViewCell.h
//  Tilling
//
//  Created by Beau Young on 19/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *suburbLabel;
@property (weak, nonatomic) IBOutlet UILabel *postCodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *phoneNumber;

@end
