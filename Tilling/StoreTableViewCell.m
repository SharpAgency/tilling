//
//  StoreTableViewCell.m
//  Tilling
//
//  Created by Beau Young on 19/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "StoreTableViewCell.h"

@implementation StoreTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
