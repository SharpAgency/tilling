//
//  Termite.h
//  Tilling
//
//  Created by Maris on 11/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Termite : UIImageView

@property (nonatomic) BOOL isDead;
@property (nonatomic, strong) NSString *moveDirection;
@property (nonatomic, strong) NSString *faceDirection;

- (id)initWithTermiteAtLocation:(CGPoint)center isHorizontal:(BOOL)isHorizontal;

- (void)faceRight:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;
- (void)faceLeft:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

- (void)faceUp:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;
- (void)faceDown:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

@end
