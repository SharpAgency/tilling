//
//  Termite.m
//  Tilling
//
//  Created by Maris on 11/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#define HORIZONTALMOVEMENT 200
#define VERTICALMOVEMENT 200

#define ANIMATEMOVEWITHDURATION 3.0

#import "Termite.h"

@implementation Termite {
    CGPoint centerGlobal;
}

- (id)initWithTermiteAtLocation:(CGPoint)center isHorizontal:(BOOL)isHorizontal {
    self = [super initWithImage:[UIImage imageNamed:@"termiteOne"]];
    if (self) {
        centerGlobal = center;

        self.frame = CGRectMake(0, 0, self.frame.size.width/1.5, self.frame.size.height/1.5);

        // Load Images
        NSArray *imageNames = @[@"termiteOne", @"termiteTwo"];
        
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (int i = 0; i < imageNames.count; i++) {
            [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
        }
        self.animationImages = images;
        self.animationDuration = 0.2;
        self.center = centerGlobal;
        [self startAnimating];
        
        self.contentMode = UIViewContentModeScaleAspectFit;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)moveToLeft:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:ANIMATEMOVEWITHDURATION
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(faceRight:finished:context:)];
                              self.center = CGPointMake(centerGlobal.x-HORIZONTALMOVEMENT, centerGlobal.y);
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"LOG");
                          }];
}

- (void)faceRight:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:1.0
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(moveToRight:finished:context:)];
                              
                              self.transform = CGAffineTransformMakeRotation(M_PI);
                              
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"Face right done");
                          }];
}

- (void)moveToRight:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:ANIMATEMOVEWITHDURATION
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(faceLeft:finished:context:)];
                              self.center = CGPointMake(centerGlobal.x+HORIZONTALMOVEMENT, centerGlobal.y);
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"Move to Right Done");
                          }];
    
}

- (void)faceLeft:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:1.0
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(moveToLeft:finished:context:)];
                              
                              self.transform = CGAffineTransformMakeRotation(0);
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"Face left done");
                          }];
}




- (void)moveUp:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:ANIMATEMOVEWITHDURATION
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(faceDown:finished:context:)];
                              self.center = CGPointMake(centerGlobal.x, centerGlobal.y - VERTICALMOVEMENT);
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"LOG");
                          }];
    
}

- (void)faceDown:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:1.0
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(moveDown:finished:context:)];
                              
                              self.transform = CGAffineTransformMakeRotation(-M_PI / 2);
                              
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"Face right done");
                          }];
}

- (void)moveDown:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:ANIMATEMOVEWITHDURATION
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(faceUp:finished:context:)];
                              self.center = CGPointMake(centerGlobal.x, centerGlobal.y + VERTICALMOVEMENT);
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"Move to Right Done");
                          }];
    
}

- (void)faceUp:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (self.isDead) return;
    
    [UIImageView animateWithDuration:1.0
                               delay:0.0
                             options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction)
                          animations:^{
                              [UIImageView setAnimationDelegate:self];
                              [UIImageView setAnimationDidStopSelector:@selector(moveUp:finished:context:)];
                              
                              self.transform = CGAffineTransformMakeRotation(M_PI / 2);
                          }
                          completion:^(BOOL finished) {
                              NSLog(@"Face left done");
                          }];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.superview];
    
    touchLocation = [[self layer] convertPoint:touchLocation toLayer:self.layer];
    

//    CGRect termiteRect = [[[self layer] presentationLayer] frame];
    if ([[self layer].presentationLayer hitTest:touchLocation]) {
        _isDead = YES;
        [self.layer removeAllAnimations];
        [self stopAnimating];
        UIImage *splatImage = [UIImage imageNamed:@"splat"];
        self.image = splatImage;
        
        NSLog(@"termite touched");
        
        [UIImageView animateWithDuration:2.0
                                   delay:0.0
                                 options:0
                              animations:^{
                                  self.alpha = 0.0;
                              }
                              completion:^(BOOL finished) {
                                  [self removeFromSuperview];
                              }];
        
        
        
    }

}






/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
