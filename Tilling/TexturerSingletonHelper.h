//
//  TexturerSingletonHelper.h
//  Tilling
//
//  Created by Beau Young on 13/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TexturerSingletonHelper : NSObject

@property (strong, nonatomic) NSString *chosenWallHeight;
@property (strong, nonatomic) NSString *guideImageName;
@property (strong, nonatomic) UIImage *photo;
@property (strong, nonatomic) NSString *wallTypeTexturePrefix;

+ (id)sharedData;

- (NSString *)getNameForOverlayGuide;

@end
