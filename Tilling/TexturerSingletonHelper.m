//
//  TexturerSingletonHelper.m
//  Tilling
//
//  Created by Beau Young on 13/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "TexturerSingletonHelper.h"

@implementation TexturerSingletonHelper

+ (id)sharedData {
    static TexturerSingletonHelper *singltonHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singltonHelper = [[self alloc] init];
    });
    return singltonHelper;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (NSString *)getNameForOverlayGuide {
    NSString *overlayName = [NSString stringWithFormat:@"%@%@.png", _guideImageName, _chosenWallHeight];
    NSLog(@"image name: %@", overlayName);
    return overlayName;
}

@end
