//
//  TexturizerViewController.h
//  Tilling
//
//  Created by Beau Young on 17/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TexturizerViewController : UIViewController

@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIImageView *overlayImageView;
@property (strong, nonatomic) UIImageView *blurredImageView;
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic) CGFloat screenHeight;
@property (strong, nonatomic) NSString *wallType;

@end
