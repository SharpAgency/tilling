//
//  TexturizerViewController.m
//  Tilling
//
//  Created by Beau Young on 17/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "TexturizerViewController.h"
#import "TexturerSingletonHelper.h"
#import "LBBlurredImage/UIImageView+LBBlurredImage.h"

@interface TexturizerViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@end

@implementation TexturizerViewController {
    NSArray *textureImageNames;
    NSArray *textureHeights;
    NSArray *textureSides;
    NSArray *textureNames;
}

- (void)viewDidLoad {
    self.screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    UIImage *background = [[TexturerSingletonHelper sharedData] photo];
    
    self.backgroundImageView = [[UIImageView alloc] initWithImage:background];
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:self.backgroundImageView];
    
    self.overlayImageView = [[UIImageView alloc] initWithFrame:self.backgroundImageView.frame];
    self.overlayImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.overlayImageView.clipsToBounds = YES;
    [self.backgroundImageView addSubview:self.overlayImageView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.2];
    self.tableView.pagingEnabled = YES;
    [self.view addSubview:self.tableView];
    
    CGRect headerFrame = [UIScreen mainScreen].bounds;
    headerFrame.size.height = [UIScreen mainScreen].bounds.size.height - 130;
    UIView *header = [[UIView alloc] initWithFrame:headerFrame];
    header.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = header;
    
    NSString *textureHeight = [NSString stringWithFormat:@"%@", [[TexturerSingletonHelper sharedData] chosenWallHeight]];
    NSString *texturePrefix = [NSString stringWithFormat:@"%@", [[TexturerSingletonHelper sharedData] wallTypeTexturePrefix]];
    
    textureNames = @[@"Stepped Profile - Horizon™ Full Right",
                     @"Stepped Profile - Horizon™ Half Right",
                     @"Stepped Profile - Horizon™ Full Left",
                     @"Stepped Profile - Horizon™ Half Left",
                     @"Block Wall – Silhouette™ Full Right",
                     @"Block Wall – Silhouette™ Half Right",
                     @"Block Wall – Silhouette™ Full Left",
                     @"Block Wall – Silhouette™ Half Left"];
    
    if ([[[TexturerSingletonHelper sharedData] wallTypeTexturePrefix] isEqualToString:@"inside"]) {
        textureImageNames = @[[NSString stringWithFormat:@"%@Stepped%@FullRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@HalfRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@FullLeft.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@HalfLeft.png", texturePrefix, textureHeight],
                              
                              [NSString stringWithFormat:@"%@Block%@FullRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@HalfRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@FullLeft.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@HalfLeft.png", texturePrefix, textureHeight],
                              ];
    }
    else if ([[[TexturerSingletonHelper sharedData] wallTypeTexturePrefix] isEqualToString:@"front"]) {
        textureNames = @[@"Stepped Profile - Horizon™ Full",
                         @"Stepped Profile - Horizon™ Top",
                         @"Stepped Profile - Horizon™ Bottom",
                         @"Block Wall - Silhouette™ Full",
                         @"Block Wall - Silhouette™ Top",
                         @"Block Wall - Silhouette™ Bottom"];
        
        textureImageNames = @[[NSString stringWithFormat:@"%@Stepped%@Full.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@Top.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@Bottom.png", texturePrefix, textureHeight],
                              
                              [NSString stringWithFormat:@"%@Block%@Full.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@Top.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@Bottom.png", texturePrefix, textureHeight],
                              ];
    }
    else if ([[[TexturerSingletonHelper sharedData] wallTypeTexturePrefix] isEqualToString:@"outside"]) {
        textureImageNames = @[[NSString stringWithFormat:@"%@Stepped%@FullRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@HalfRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@FullLeft.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Stepped%@HalfLeft.png", texturePrefix, textureHeight],
                              
                              [NSString stringWithFormat:@"%@Block%@FullRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@HalfRight.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@FullLeft.png", texturePrefix, textureHeight],
                              [NSString stringWithFormat:@"%@Block%@HalfLeft.png", texturePrefix, textureHeight],
                              ];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [textureNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
    cell.textLabel.textColor = [UIColor whiteColor];

    cell.textLabel.text = [textureNames objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.overlayImageView setImage:[UIImage imageNamed:textureImageNames[indexPath.row]]];
    self.overlayImageView.alpha = 0.7f;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect bounds = self.view.bounds;
    
    self.backgroundImageView.frame = bounds;
    self.overlayImageView.frame = bounds;
    self.tableView.frame = bounds;
}

#pragma mark - Save button
- (IBAction)saveButton:(UIBarButtonItem *)sender {

    UIGraphicsBeginImageContext(self.backgroundImageView.frame.size);
	[self.backgroundImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

    UIImageWriteToSavedPhotosAlbum(viewImage,
                                   self,
                                   @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:),
                                   NULL);
}

- (void)savedPhotoImage:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    NSString *message = @"This image has been saved to your Photos album";
    if (error) {
        message = [error localizedDescription];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
