//
//  ViewController.m
//  Tilling
//
//  Created by Maris on 2/10/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "ViewController.h"
#import "SWRevealViewController.h"
#import "ArchitecturalTableViewCell.h"
#import "TillingBuildAHomeViewController.h"

@interface ViewController () {
    NSIndexPath *selectedCellIndexPath;
    NSArray *backgroundColorArray;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set datasource and delegates for tableview
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.302 green:0.306 blue:0.357 alpha:1.000];
    
    // set up image array
    backgroundColorArray = @[[UIColor colorWithRed:0.258 green:0.260 blue:0.307 alpha:0.600],
                             [UIColor colorWithRed:0.369 green:0.376 blue:0.427 alpha:0.600],
                             [UIColor colorWithRed:0.416 green:0.424 blue:0.478 alpha:0.600],
                             [UIColor colorWithRed:0.459 green:0.467 blue:0.533 alpha:0.600],
                             [UIColor colorWithRed:0.506 green:0.518 blue:0.588 alpha:0.600],
                             [UIColor colorWithRed:0.553 green:0.569 blue:0.643 alpha:0.600]];
    
    // set the side bar button action
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // set shadow thrown on rear view controller
    self.revealViewController.frontViewShadowOffset = CGSizeMake(1, 0);
    self.revealViewController.frontViewShadowRadius = 20;
    self.revealViewController.frontViewShadowOpacity = .3;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // set cell identifier
    static NSString *CellIdentifier = @"Cell";
    
    // implement custom uitableviewcell
    ArchitecturalTableViewCell *cell = (ArchitecturalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ArchitecturalTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    // Arrays containing content for cells - add to these and add more rows when needing more content
    NSArray *titleStrings = @[@"About Us", @"Chain of Custody", @"Build a Home", @"Certificates", @"Visit this sections website"];
    NSArray *detailStrings = @[
                               @"The Tilling Group comprises a number of smaller business units. These include; the SmartFrame division, which caters to the domestic and multi-residential markets. The FrameSmart unit accommodates all framing solutions, including the treatment of materials for termite prone zones. Our SmartStruct division focuses on the commercial markets, dealing with large engineered solutions to meet the growing demands of a modern construction era. The Architectural products division utilise traditional timber milling and profiling methods to create and beautiful products delivering the natural aesthetic of timber to interior spaces. Overarching the entire group is our engineering and design centre. This centre is cable of providing full engineering services, specifically focusing on timber (re)engineering in lieu of alternative material construction, estimation and customised design.\n\nThe Tilling Group was established in November 1963 and began as a small factory in Eltham Victoria. The company now has large, modern offices and warehouses in most major cities across Australia, as well as manufacturing facilities at its head office in Kilsyth, Victoria, and treatment facilities in Queensland, New South Wales and Western Australia.\n\nSmartFrame\nThe company also carries its own unique range of Engineered Wood Products known as SmartFrame. Products in this range include SmartLam glued laminated beams, Smart LVL, SmartJoist I-joists plus the state of the art software program and technical library in the SmartFrame Design Compendium. A fully automated SmartFrame cutting system which provides pre-cut penetrations in SmartJoists has recently been installed. The company distributes its products on a wholesale basis only, through its national network of sales offices and distribution warehouses.\n\nFrameSmart\nUnfortunately Australia’s termite problem continues to grow from year to year. Termites are a major contributor to the degradation of structural support materials in our homes. Including the European House Borer (EHB), which is a serious pest of untreated dry softwood, including pinewood, particularly in Western Australia. Without appropriate action, EHB has the potential to infest homes built with untreated pinewood structural timber. FrameSmart pine framing products have the option of being treated with SmartGuard. SmartGuard termite treatments assist in protecting your building, speak to our friendly customer service team to ¬find out more regarding treatment options, call 1300 668 690.\n\nSmartStruct\nPresently there is considerable interest in the construction of large-scale projects utilising timber. Timber is an ideal alternative to current construction methods with considerable environmental advantages, such as the storage of Carbon and Recyclability. A primary reason for building in timber is the speed of assembly. Timber has been found to be quicker to construct and easier to work with, not to mention considerable advantages in weight reduction with comparable strength to alternative materials.\n\nArchitectural Products\nThe product range includes solid timber lining & cladding profiles, shingles, shakes, finished mouldings and pre-finished and a range of colour options. The prefinished lining boards are pre-finished with four coats of polyurethane in a satin finish. So once installed, the job is done. No smells, no mess - it's that simple.\n\nSmartFrame Design Centre\nThe team is made up of Engineers, Building Designers, Builders, Building Surveyors and F&T Detailers all working together to provide an integrated solution for your project requirements. Services provided by the SmartFrame Design Centre include; Full engineering support on all SmartFrame EWP - Free design and take off from building plans - Dedicated toll free engineering support line - Structured SmartFrame software training program (Approved Vic building commission CPD provider) - Certification of engineering designs to relevant state Building acts and regulations.\n\nManufacturing\nEquipment includes dehumidifying drying kilns with a combined charge capacity of 370 cube metres, hydromat Weinig moulders with a combined capacity on a one shift basis of 65,000 lineal metres, and a unique multi-million dollar continuous pre-finishing line with a capacity of 30,000 lineal metres on a two shift basis. A fully automated SmartFrame cutting system which provides pre-cut penetrations in SmartJoists has recently been installed. The company distributes its products on a wholesale basis only, through its national network of sales offices and distribution warehouses.",
                               
                               @"Chain of Custody (CoC) tracks the path taken by a forest product from its origin in a certified forest, right through to its end use by the consumer. \n\n It includes every link in the supply chain - such as harvesting, transportation, primary and secondary processing, manufacturing, re-manufacturing, distribution and sales. It means that when buying forest products, customers can select certified products from a traceable source.",
                               
                               @"",
                               @"",
                               @""];
    
    // set the label in Nib
    cell.titleLabel.text = [titleStrings objectAtIndex:indexPath.section];
    cell.detailLabel.text = [detailStrings objectAtIndex:indexPath.section];
    cell.backgroundColor =  [backgroundColorArray objectAtIndex:indexPath.section];
    
    return cell;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"accepted"forKey:@"keyDisclaimer"];
    [defaults synchronize];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates]; // tell the table you're about to start making changes
    
    // If the index path of the currently expanded cell is the same as the index that
    // has just been tapped set the expanded index to nil so that there aren't any
    // expanded cells, otherwise, set the expanded index to the index that has just
    // been selected.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) selectedCellIndexPath = nil;
    else selectedCellIndexPath = indexPath;
    
    [tableView endUpdates]; // tell the table you're done making your changes
    
    if (indexPath.section == 2) {
        [self performSegueWithIdentifier:@"BuildAHomeSegue" sender:nil];
    }
    if (indexPath.section == 3) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tilling.com.au/chain-of-custody-overview"]];
    }
    if (indexPath.section == 4) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tilling.com.au"]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Compares the index path for the current cell to the index path stored in the expanded
    // index path variable. If the two match, return a height of 100 points, otherwise return
    // a height of 44 points.
    
    if ([indexPath compare:selectedCellIndexPath] == NSOrderedSame) {
        if (indexPath.section == 0) return 2120.0; // Expanded height
        else if (indexPath.section == 1) return 270;
    }
    return 56; // Normal height
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 168;
    }
    return 0; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"BuildAHomeSegue"]) {
        TillingBuildAHomeViewController *buildAHomeController = [segue destinationViewController];
        buildAHomeController.tableView = nil;
    }
}
@end
