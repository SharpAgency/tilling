//
//  WallHeightViewController.m
//  Tilling
//
//  Created by Beau Young on 16/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "WallHeightViewController.h"
#import "TexturerSingletonHelper.h"

@interface WallHeightViewController ()

@end

@implementation WallHeightViewController

- (IBAction)heightBtn:(UIButton *)sender {
    [[TexturerSingletonHelper sharedData] setChosenWallHeight:@"2.4m"];
    [self performSegueWithIdentifier:@"cameraSegue" sender:self];
}

@end
