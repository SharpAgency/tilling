//
//  WallTypeViewController.m
//  Tilling
//
//  Created by Beau Young on 13/02/2014.
//  Copyright (c) 2014 Sharp Agency. All rights reserved.
//

#import "WallTypeViewController.h"
#import "TexturerSingletonHelper.h"

#define outsideWall @"outsideOverlayImage"
#define insideWall @"insideOverlayImage"
#define frontWall @"frontOverlayImage"

@implementation WallTypeViewController

- (IBAction)frontWallBtn:(UIButton *)sender {
    [[TexturerSingletonHelper sharedData] setGuideImageName:frontWall];
    [[TexturerSingletonHelper sharedData] setWallTypeTexturePrefix:@"front"];
    [self performSegueWithIdentifier:@"heightSegue" sender:self];
}

- (IBAction)insideWallBtn:(UIButton *)sender {
    [[TexturerSingletonHelper sharedData] setGuideImageName:insideWall];
    [[TexturerSingletonHelper sharedData] setWallTypeTexturePrefix:@"inside"];
    [self performSegueWithIdentifier:@"heightSegue" sender:self];
}

- (IBAction)outsideWallBtn:(UIButton *)sender {
    [[TexturerSingletonHelper sharedData] setGuideImageName:outsideWall];
    [[TexturerSingletonHelper sharedData] setWallTypeTexturePrefix:@"outside"];
    [self performSegueWithIdentifier:@"heightSegue" sender:self];
}

@end
